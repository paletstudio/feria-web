'use strict';

/**
 * @ngdoc directive
 * @name sytevApp.directive:formFieldText
 * @description
 * # formFieldText
 */
angular.module('feriaApp')
    .directive('formFieldText', function() {
        var template = '<div class="form-group" ng-class="{\'has-error\' : form.{{name}}.$invalid && ngRequired, \'has-success\':form.{{name}}.$valid && ngRequired }">';
        template += '<label ng-class="{\'col-md-3\' : horizontal, \'control-label\': true}">{{label}}</label>';
        template += '<div ng-class="{\'col-md-9\' : horizontal}">';
        template += '<input pattern="{{pattern}}" ng-model="ngModel" ng-trim="ngTrim" ng-required="ngRequired" ng-disabled="ngDisabled" name="{{name}}" type="{{inputType}}" placeholder="{{placeholder}}" tabindex={{tabIndex}} class="form-control">';
        template += '</div>';
        template += '</div>';
        return {
            template: template,
            restrict: 'E',
            scope: {
                name: '@',
                ngModel: '=ngModel',
                formStyle: '@',
                label: '@',
                placeholder: '@',
                ngRequired: '=?',
                ngShow: '=?',
                ngHide: '=?',
                ngIf: '=?',
                ngDisabled: '=?',
                ngTrim: '=?',
                pattern: '@',
                ngChange: '&',
                type: '=?',
                tabIndex: '@'
            },
            require: ['^form', 'ngModel'],
            link: function postLink(scope, element, attrs, controllers) {
                scope.form = controllers[0];
                scope.horizontal = true;
                scope.placeholder = scope.label;
                scope.inputType = 'text';

                scope.$watch('ngModel', function() {
                    if (scope.ngChange) {
                        scope.ngChange();
                    }
                });

                attrs.$observe('formStyle', function(value) {
                    if (value === 'vertical') {
                        scope.horizontal = false;
                    }
                });

                attrs.$observe('placeholder', function(value) {
                    scope.placeholder = value;
                });

                attrs.$observe('type', function(value){
                    if(value === 'password'){
                        scope.inputType = 'password';
                    }
                });
            }
        };
    });
