'use strict';
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.auditor', {
                url: 'auditor',
                controller: 'AuditorCtrl as vm',
                templateUrl: 'views/auditor.form.html',
                loginRequired: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Auditor'
                },
                resolve: {
                    _auditors: ['user', function(user) {
                        return user.getAuditors();
                    }],
                    _mods: ['user', function(user) {
                        return user.getMods();
                    }]
                }
            });
    })
    .controller('AuditorCtrl', function($state, toastr, _auditors, _mods, auditor) {
        var vm = this;
        var id = $state.params.id;
        vm.tituloPagina = $state.current.data.title;
        vm.auditors = _auditors;
        vm.mods = _mods;
        vm.save = save;
        vm.auditor = '';
        vm.auditorEmployees = [];
        vm.preselectEmployees = preselectEmployees;

        function save() {
            auditor.assignEmployees(vm.auditor, vm.auditorEmployees)
                .then(function(res) {
                    toastr.success(res.msg, 'Éxito');
                });
        }

        function preselectEmployees(auditor) {
            vm.auditorEmployees = [];
            if (auditor.employees.length > 0) {
                auditor.employees.forEach(function(e){
                    var exist = vm.mods.find(function(obj) {
                        return obj.id === e.employee_id;
                    });
                    if (exist) {
                        vm.auditorEmployees.push(exist);
                    }
                });
            }
        }
    });
