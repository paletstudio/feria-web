'use strict';

/**
 * @ngdoc filter
 * @name feriaApp.filter:reverse
 * @function
 * @description
 * # reverse
 * Filter in the feriaApp.
 */
angular.module('feriaApp')
  .filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
});
