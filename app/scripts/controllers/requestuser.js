'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:RequestuserCtrl
 * @description
 * # RequestuserCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.request-user', {
                url: 'request/user',
                controller: 'RequestUserCtrl as vm',
                templateUrl: 'views/requestuser.html',
                loginRequired: true,
                /*
                      validateAccess: true,
                      cache: false,*/
                data: {
                    title: 'Mis sugerencias'
                },
                resolve: {
                    _request: function(request) {
                        return request.getRequestUser({
                            paginate: true
                        });
                    },
                    _types: function(request) {
                        return request.getRequestTypes();
                    },
                }
            });
    })
    .controller('RequestUserCtrl', function(request, $state, _request, _types) {
        var vm = this;
        vm.tituloPagina = $state.current.data.title;
        vm.requests = _request.data;
        vm.totalItems = _request.total;
        vm.requestUserInfo = requestUserInfo;
        vm.filter = {};
        vm.types = _types;
        vm.filter.type = 1;
        vm.filterRequests = filterRequests;
        vm.reload = filterRequests;

        function filterRequests() {
            getRequests({
                type: vm.filter.type
            });
        }

        function getRequests(params) {
            return request.getRequestUser({
                    type: vm.filter.type,
                    paginate: true,
                    page: params.page || 1
                })
                .then(function(res) {
                    vm.requests = res.data;
                    vm.totalItems = res.total;
                });
        }

        function requestUserInfo(request) {
            return ['<p class="request-user-info"><b>Nombre: </b>' + request.applicant_name + '</p>',
                '<p class="request-user-info"><b>Apellido Paterno: </b> ' + request.applicant_family_name + ' </p>',
                '<p class="request-user-info"><b>Apellido Materno: </b> ' + request.applicant_family_name_2 + ' </p>',
                '<p class="request-user-info"><b>No. tarjeta: </b>' + request.applicant_identifier + '</p>',
                '<p><b><i class="fa fa-phone" aria-hidden="true"></i>: </b>' + request.applicant_phone + '</p>',
                '<p><b><i class="fa fa-envelope" aria-hidden="true"></i>: </b>' + request.applicant_email + '</p>'
            ].join('');
        }
    });
