'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:OxxoformCtrl
 * @description
 * # OxxoformCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.oxxo-new', {
                url: 'oxxo/new',
                controller: 'OxxoFormCtrl as vm',
                templateUrl: 'views/oxxo.form.html',
                loginRequired: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Oxxo > Nuevo'
                }
            })
            .state('app.oxxo-edit', {
                url: 'oxxo/{id:int}',
                controller: 'OxxoFormCtrl as vm',
                templateUrl: 'views/oxxo.form.html',
                loginRequired: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Oxxo > Editar'
                }
            });
    })
    .controller('OxxoFormCtrl', function(oxxo, $state, toastr, sweetAlert) {
        var vm = this;
        var id = $state.params.id;
        vm.tituloPagina = $state.current.data.title;
        vm.oxxo = oxxo.init();

        function getOxxo(id) {
            return oxxo.get(id)
                .then(function(res) {
                    if (res) {
                        vm.oxxo = res;
                    } else {
                        $state.go('app.oxxo-new');
                    }
                });
        }

        if (id) {
            getOxxo(id);
        }

        vm.save = function() {
            oxxo.save(vm.oxxo)
                .then(function(res) {
                    toastr.success(res.data.msg, 'Exito');
                    if (!vm.oxxo.id) vm.clean();
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                });
        };

        vm.clean = function() {
            vm.oxxo = oxxo.init();
        };

        vm.delete = function() {
            return sweetAlert.confirm('¿Está seguro?')
                .then(function() {
                    return oxxo.delete(vm.oxxo)
                        .then(function(res) {
                            $state.go('app.oxxo');
                            toastr.success(res.data.msg, 'Éxito');
                        });
                });
        };

    });
