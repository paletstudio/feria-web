'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:CategoryformCtrl
 * @description
 * # CategoryformCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.category-new', {
                url: 'category/new',
                controller: 'CategoryFormCtrl as vm',
                templateUrl: 'views/category.form.html',
                loginRequired: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Categoría > Nueva'
                }
            })
            .state('app.category-edit', {
                url: 'category/{id:int}',
                controller: 'CategoryFormCtrl as vm',
                templateUrl: 'views/category.form.html',
                loginRequired: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Categoría > Editar'
                }
            });
    })
    .controller('CategoryFormCtrl', function(category, $state,toastr, sweetAlert) {
        var vm = this;
        var id = $state.params.id;
        vm.tituloPagina = $state.current.data.title;
        vm.category = category.init();
        vm.clean = clean;
        vm.save = save;

        vm.categoryTypes = category.getCategoryTypes();

        function getCategory(id) {
            return category.get(id)
                .then(function(res) {
                    if(res){
                        vm.category = res;
                    } else {
                        $state.go('app.category-new');
                    }
                });
        }

        if (id) {
            getCategory(id);
        }

        function save() {
            category.save(vm.category)
                .then(function(res) {
                    toastr.success(res.data.msg, 'Éxito');
                    if (!vm.category.id) clean();
                })
                .catch(function(err){
                    toastr.error(err, 'Error');
                });
        }

        function clean() {
            vm.category = category.init();
        }

        vm.delete = function() {
            sweetAlert.confirm('¿Está seguro?')
            .then(function() {
                category.delete(vm.category)
                .then(function(res) {
                    toastr.success(res.data.msg, 'Éxito');
                    $state.go('app.category');
                });
            });
        };

    });
