'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:EstacionCtrl
 * @description
 * # EstacionCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.estacion', {
                url: 'estacion',
                controller: 'EstacionCtrl as vm',
                templateUrl: 'views/estacion.html',
                loginRequired: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Estación'
                },
                resolve: {
                    _estacion: function(estacion){
                        return estacion.getAll();
                    }
                }
            });
    })
    .controller('EstacionCtrl', function(estacion, $state, _estacion) {
        var vm = this;
        vm.tituloPagina = $state.current.data.title;
        vm.reload = getEstaciones;
        vm.estaciones = _estacion;

        function getEstaciones() {
            return estacion.getAll()
                .then(function(res) {
                    vm.estaciones = res;
                });
        }

    });
