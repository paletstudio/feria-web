'use strict';

describe('Controller: RutaformCtrl', function () {

  // load the controller's module
  beforeEach(module('feriaApp'));

  var RutaformCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RutaformCtrl = $controller('RutaformCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
