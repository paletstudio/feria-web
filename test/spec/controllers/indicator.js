'use strict';

describe('Controller: IndicatorctrlCtrl', function () {

  // load the controller's module
  beforeEach(module('feriaApp'));

  var IndicatorctrlCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    IndicatorctrlCtrl = $controller('IndicatorctrlCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
