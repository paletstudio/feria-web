'use strict';

/**
 * @ngdoc service
 * @name feriaApp.category
 * @description
 * # category
 * Service in the feriaApp.
 */
angular.module('feriaApp')
    .service('category', function(API, $q, toastr, utilsService) {
        var me = this;
        var url = 'category';

        me.init = function() {
            return {
                id: 0,
                name: null,
                description: null,
                active: 1
            };
        };

        me.getAll = function(params) {
            var endpoint = url;
            if (params) {
                if (params.info) endpoint = utilsService.updateQueryStringParameter(endpoint, 'info', true);
                if (params.type) endpoint = utilsService.updateQueryStringParameter(endpoint, 'type', params.type);
            }

            return API.get(endpoint)
                .then(function(res) {
                    if (res.data.rows && res.data.rows.length > 0) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.getAllByRoute = function() {
            return API.get(url + '/route')
                .then(function(res) {
                    if (res.data.rows && res.data.rows.length > 0) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.getAllPublic = function(params) {
            var endpoint = 'public/' + url;
            if (params) {
                if (params.info) endpoint = utilsService.updateQueryStringParameter(endpoint, 'info', true);
                if (params.type) endpoint = utilsService.updateQueryStringParameter(endpoint, 'type', params.type);
            }
            return API.get(endpoint, {
                    skipAuthorization: true
                })
                .then(function(res) {
                    if (res.data.rows && res.data.rows.length > 0) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.get = function(id) {
            return API.get(url + '/' + id)
                .then(function(res) {
                    if (res.data.rows && res.data.rows[0]) {
                        return $q.resolve(res.data.rows[0]);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(null);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.create = function(category) {
            return API.post(url, category);
        };

        me.update = function(category) {
            return API.put(url + '/' + category.id, category);
        };

        me.delete = function(category) {
            return API.delete(url + '/' + category.id);
        };

        me.save = function(category) {
            if (category.id) {
                return me.update(category);
            } else {
                return me.create(category);
            }
        };

        me.getCategoryTypes = function() {
            return [{
                id: 1,
                name: 'Pública'
            }, {
                id: 2,
                name: 'Interna'
            }]
        };

    });
