'use strict';

describe('Directive: evTable', function () {

  // load the directive's module
  beforeEach(module('feriaApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<ev-table></ev-table>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the evTable directive');
  }));
});
