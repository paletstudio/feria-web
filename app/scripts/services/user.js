'use strict';

/**
 * @ngdoc service
 * @name feriaApp.user
 * @description
 * # user
 * Service in the feriaApp.
 */
angular.module('feriaApp')
    .service('user', function(API, $q, toastr, locker) {

        var me = this;
        var url = 'user';

        me.init = function() {
            return {
                id: 0,
                name: null,
                family_name: null,
                family_name_2: null,
                username: null,
                email: null,
                password: null,
                workingplace_id: null,
                jobposition_id: null,
                requestorigin_id: null,
                profile_id: null,
                active: 1
            };
        };

        me.getAll = function(info) {
            var query = '';
            if (info) {
                query = '?info=true';
            }
            return API.get(url + query)
                .then(function(res) {
                    if (res.data.rows && res.data.rows.length > 0) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.get = function(id) {
            return API.get(url + '/' + id)
                .then(function(res) {
                    if (res.data.rows && res.data.rows[0]) {
                        return $q.resolve(res.data.rows[0]);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(null);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.getMods = function() {
            return API.get(url + '/moderator')
                .then(function(res) {
                    return $q.resolve(res.data.rows);
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.getAuditors = function() {
            return API.get(url + '/auditor')
                .then(function(res) {
                    return $q.resolve(res.data.rows);
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.getEmployees = function(id) {
            return API.get(url + '/' + id + '/employee')
                .then(function(res) {
                    return $q.resolve(res.data);
                })
                .catch(function(err) {
                    return $q.reject(err);
                });
        };

        me.getRutas = function() {
            return API.get(url + '/ruta')
                .then(function(res) {
                    if (res.data.rows && res.data.rows.length > 0) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.changePassword = function(user) {
            var userId = locker.get('user') ? locker.get('user').id : null;
            return API.put(url + '/' + userId + '/password', user)
                .then(function(res) {
                    return $q.resolve(res.data);
                });
        };

        me.create = function(user) {
            return API.post(url, user);
        };

        me.update = function(user) {
            return API.put(url + '/' + user.id, user);
        };

        me.save = function(user) {
            if (user.id) {
                return me.update(user);
            } else {
                return me.create(user);
            }
        };

        me.delete = function(userId) {
            return API.delete(url + '/' + userId);
        };

    });
