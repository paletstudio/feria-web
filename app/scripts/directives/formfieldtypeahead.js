(function() {
    'use strict';

    angular
        .module('feriaApp')
        .directive('formFieldTypeahead', formFieldTypeahead);

    /* @ngInject */
    function formFieldTypeahead() {
        var template = '<div class="form-group" ng-class="{\'has-error\' : form.{{vm.name}}.$invalid, \'has-success\':form.{{vm.name}}.$valid }">';
        template += '<label ng-class="{\'col-md-3\' : vm.horizontal, \'control-label\': true}">{{vm.label}}</label>';
        template += '<div ng-class="{\'col-md-9\' : vm.horizontal}">';
        template += '<input name="{{vm.name}}" type="text" placeholder="{{vm.placeholder}}" ng-model="vm.innerModel" uib-typeahead="row as row.name for row in vm.rows | filter:$viewValue | limitTo:8" class="form-control">';
        template += '</div>';
        template += '</div>';

        var directive = {
            restrict: 'E',
            template: template,
            scope: {
                name: '@',
                ngModel: '=ngModel',
                formStyle: '@',
                label: '@',
                placeholder: '@',
                ngRequired: '=?',
                ngChange: '&',
                tabIndex: '@',
                catalog: '@',
                skipAuthorization: '=?'
            },
            require: ['^form', 'ngModel'],
            link: linkFunc,
            controller: FormFieldTypeaheadController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        function linkFunc(scope, el, attr, ctrl) {
            scope.form = ctrl[0];
            scope.vm.horizontal = true;
            // scope.placeholder = scope.label;

            scope.$watch('vm.innerModel', function(newValue) {
                if (angular.isObject(newValue)) {
                    scope.form[scope.vm.name].$setValidity('', true);
                    scope.vm.ngModel = {
                        value: newValue.name,
                        value_id: newValue.id
                    };
                    if (scope.ngChange) {
                        scope.ngChange();
                    }
                } else {
                    scope.vm.ngModel = null;
                    scope.form[scope.vm.name].$setValidity('', false);
                }

            });
        }
    }

    FormFieldTypeaheadController.$inject = ['API'];

    /* @ngInject */
    function FormFieldTypeaheadController(API) {
        var vm = this;

        activate();

        function activate() {
            getCatalog();
        }

        function getCatalog() {
            if (vm.skipAuthorization) vm.catalog = 'public/' + vm.catalog;
            API.get(vm.catalog)
                .then(function(res) {
                    vm.rows = res.data.rows;
                });
        }
    }
})();
