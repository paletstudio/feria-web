'use strict';

/**
 * @ngdoc overview
 * @name feriaApp
 * @description
 * # feriaApp
 *
 * Main module of the application.
 */
angular
    .module('feriaApp', [
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.router',
        'toastr',
        'angularMoment',
        'ui.select',
        'ui.bootstrap',
        'anim-in-out',
        'angular-jwt',
        'angular-locker',
        'dataGrid',
        'pagination',
        'naif.base64',
        'btford.socket-io',
        'localytics.directives',
        'nzToggle',
        'oitozero.ngSweetAlert',
        'ngLodash',
        'chart.js',
        'ui.utils.masks',
        'angular-loading-bar'
    ])
    .config(function($stateProvider, $urlRouterProvider, $httpProvider, toastrConfig, lockerProvider, jwtOptionsProvider, ChartJsProvider, cfpLoadingBarProvider) {
        cfpLoadingBarProvider.latencyThreshold = 500;
        
        String.prototype.capitalize = function() {
            return this.charAt(0).toUpperCase() + this.slice(1);
        };

        ChartJsProvider.setOptions({
            tooltips: {
                displayColors: false
            },
            legend: {
                display: true,
                position: 'bottom'
            }
        });

        angular.extend(toastrConfig, {
            autoDismiss: true,
            containerId: 'toast-container',
            maxOpened: 5,
            newestOnTop: true,
            positionClass: 'toast-top-right',
            preventDuplicates: false,
            preventOpenDuplicates: false,
            target: 'body'
        });

        lockerProvider.defaults({
            driver: 'local',
            namespace: 'feria',
            separator: '_',
            eventsEnabled: true,
            extend: {}
        });

        jwtOptionsProvider.config({
            whiteListedDomains: ['localhost', '158.69.202.20'],
            tokenGetter: ['options', 'locker', 'jwtHelper', 'API', 'settings', 'session', function(options, locker, jwtHelper, API, settings, session) {
                var jwt = locker.get('token');
                if (!options || options.url.substr(options.url.length - 5) === '.html') {
                    return null;
                }
                if (jwt && jwtHelper.isTokenExpired(jwt)) {
                    return API.get('refreshtoken', {
                        skipAuthorization: true,
                        headers: {
                            Authorization: 'Bearer ' + jwt
                        },
                    }).then(function(response) {
                            locker.put('token', response.data.rows);
                            return response.data.rows;
                        },
                        function(response) {
                            session.logout();
                        });
                } else {
                    return jwt;
                }
            }],
            // unauthenticatedRedirector: ['session', 'locker', function(session, locker) {
            //     if (locker.get('token')) {
            //         session.logout();
            //     }
            // }]
        });

        $httpProvider.interceptors.push('jwtInterceptor');
        $urlRouterProvider.otherwise('/');
        //
        $stateProvider
            .state('login', {
                url: '/login',
                controller: 'LoginCtrl as vm',
                templateUrl: 'views/login.html',
                cache: true
            })
            .state('app', {
                abstract: true,
                url: '/',
                loginRequired: true,
                templateUrl: 'views/main.html',
            });
    })
    .run(function($rootScope, $location, $state, session, authManager) {
        $rootScope.$on('$stateChangeStart', function(event, toState) {
            if (toState.url === '/login' && session.isLogged()) {
                event.preventDefault();
                $state.go('app.inicio');
                return;
            }

            if (toState.loginRequired && !session.isLogged()) {
                event.preventDefault();
                $rootScope.returnToState = '/' + toState.url;
                $state.go('login');
                return;
            }

            if (toState.blacklist && angular.isArray(toState.blacklist)) {
                toState.blacklist.forEach(function(b) {
                    if (session.isInRole(b)) {
                        event.preventDefault();
                        return;
                    }
                });
                /*if (!(opcion.validateAccess(toState.name))) {
                    event.preventDefault();
                    $state.go('rebotin.inicio');
                }*/
            }
        });

        authManager.checkAuthOnRefresh();
        authManager.redirectWhenUnauthenticated();
    });
