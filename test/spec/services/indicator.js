'use strict';

describe('Service: Indicator', function () {

  // load the service's module
  beforeEach(module('feriaApp'));

  // instantiate service
  var Indicator;
  beforeEach(inject(function (_Indicator_) {
    Indicator = _Indicator_;
  }));

  it('should do something', function () {
    expect(!!Indicator).toBe(true);
  });

});
