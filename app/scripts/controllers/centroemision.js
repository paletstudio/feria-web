'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:CentroemisionCtrl
 * @description
 * # CentroemisionCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.centroEmision', {
                url: 'centroemision',
                controller: 'CentroEmisionCtrl as vm',
                templateUrl: 'views/centroemision.html',
                loginRequired: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Centro de emisión'
                },
                resolve:{
                    _centroemision: function(centroEmision){
                        return centroEmision.getAll();
                    }
                }
            });
    })
    .controller('CentroEmisionCtrl', function(centroEmision, $state, _centroemision) {
        var vm = this;
        vm.tituloPagina = $state.current.data.title;
        vm.reload = getCentrosEmision;
        vm.centrosEmision = _centroemision;

        function getCentrosEmision() {
            return centroEmision.getAll()
                .then(function(res) {
                    vm.centrosEmision = res;
                });
        }

    });
