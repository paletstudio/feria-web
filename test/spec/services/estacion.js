'use strict';

describe('Service: estacion', function () {

  // load the service's module
  beforeEach(module('feriaApp'));

  // instantiate service
  var estacion;
  beforeEach(inject(function (_estacion_) {
    estacion = _estacion_;
  }));

  it('should do something', function () {
    expect(!!estacion).toBe(true);
  });

});
