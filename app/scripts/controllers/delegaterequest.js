'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:DelegaterequestCtrl
 * @description
 * # DelegaterequestCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .controller('DelegateRequestCtrl', function($scope, $uibModalInstance, request, users, API, toastr) {
        var vm = this;
        var url = 'request';
        vm.request = request;
        vm.cancel = cancel;
        vm.users = users;
        vm.save = save;

        function cancel() {
            $uibModalInstance.dismiss(null);
        }

        function save(request_id, user_id) {
            return API.post(url + '/assign', {
                    request_id: request_id,
                    user_id: user_id
                })
                .then(function(res) {
                    $scope.$emit('request.delegate');
                    $uibModalInstance.close();
                    toastr.success(res.data.msg, 'Éxito');
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                });
        }
    });
