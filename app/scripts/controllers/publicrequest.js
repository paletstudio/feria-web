'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:PublicrequestCtrl
 * @description
 * # PublicrequestCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('public-request-new', {
                url: '/public/request',
                controller: 'RequestPublicCtrl as vm',
                templateUrl: 'views/public/request.form.html',
                loginRequired: false,
                /*
                               validateAccess: true,
                               cache: false,*/
                data: {
                    title: 'Registrar sugerencia'
                }
            });
    })
    .controller('RequestPublicCtrl', function($state, category, subcategory, publicrequest, centroEmision, oxxo, toastr, ruta, estacion) {
        var vm = this;
        var selectedCategory;


        vm.identifierOptions = getIdentifierOptions();
        vm.identifier = vm.identifierOptions[1];
        vm.dpOpened = false;
        vm.maxDate = moment().toDate();
        vm.minDate = moment().subtract(3, 'month').toDate();
        
        vm.tituloPagina = $state.current.data.title;
        vm.request = publicrequest.init();
        vm.categoryChanged = categoryChanged;
        vm.clean = clean;
        vm.save = save;

        vm.prepareRequestData = function() {
            if (!selectedCategory || selectedCategory !== vm.request.subcategory.category_id) {
                selectedCategory = vm.request.subcategory.category_id;
                vm.request.data = vm.request.subcategory.data.map(function(d) {
                    return {
                        data: d
                    };
                });
            }
        };

        getCategories();

        function getCategories() {
            category.getAllPublic({
                type: 1, info: true
            })
                .then(function(res) {
                    vm.categories = res;
                });
        }

        function categoryChanged() {
            vm.request.data = [];
            vm.request.subcategory = null;
        }

        function clean() {
            vm.request = publicrequest.init();
        }

        function save() {
            setNotVisibleValues();
            publicrequest.create(vm.request)
                .then(function(res) {
                    toastr.success(res.data.msg, 'Éxito');
                    if (!vm.request.id) clean();
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                });
        }

        function setNotVisibleValues() {
            vm.request.application_datetime = moment().format();
            vm.request.subcategory_id = vm.request.subcategory.id;
            vm.request.status_id = 1;
            vm.request.applicant_is_folio = vm.identifier.id;
            vm.request.user_id = null;
            vm.request.requestorigin_id = 2;
            vm.request.request_type_id = 1;

        }

        function getIdentifierOptions() {
            return [{
                id: 0,
                name: 'Tarjeta',
                pattern: '\\d{10}',
                placeholder: 'Número de tarjeta (10 dígitos)'
            }, {
                id: 1,
                name: 'Folio',
                pattern: '\[a-zA-Z]{1}\\d{6,7}',
                placeholder: 'Folio de la tarjeta (1 letra y 7 dígitos)'
            }];
        }
    });
