'use strict';

describe('Directive: formFieldText', function () {

  // load the directive's module
  beforeEach(module('feriaApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<form-field-text></form-field-text>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the formFieldText directive');
  }));
});
