'use strict';

describe('Controller: RequestuserCtrl', function () {

  // load the controller's module
  beforeEach(module('feriaApp'));

  var RequestuserCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RequestuserCtrl = $controller('RequestuserCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
