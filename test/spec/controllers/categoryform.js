'use strict';

describe('Controller: CategoryformCtrl', function () {

  // load the controller's module
  beforeEach(module('feriaApp'));

  var CategoryformCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CategoryformCtrl = $controller('CategoryformCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(CategoryformCtrl.awesomeThings.length).toBe(3);
  });
});
