'use strict';

angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.employee', {
                url: 'employee',
                controller: 'EmployeeCtrl as vm',
                templateUrl: 'views/employees.html',
                loginRequired: true,
                blacklist: [
                    'user', 'admin'
                ],
                data: {
                    title: 'Empleados'
                }
            });
    })
    .controller('EmployeeCtrl', function($state, locker, user) {
        var vm = this;
        vm.tituloPagina = $state.current.data.title;
        vm.id = locker.get('user').id;
        getEmployees();

        function getEmployees() {
            user.getEmployees(vm.id)
                .then(function(res) {
                    vm.user = res.rows;
                });
        }
    });
