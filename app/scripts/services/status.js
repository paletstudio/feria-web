'use strict';

/**
 * @ngdoc service
 * @name feriaApp.status
 * @description
 * # status
 * Service in the feriaApp.
 */
angular.module('feriaApp')
  .service('status', function(API, toastr,$q) {
    var me = this;
    var url = 'status';

    me.getAll = function() {
      return API.get(url)
        .then(function(res) {
          if (res.data.rows && res.data.rows.length > 0) {
            return $q.resolve(res.data.rows);
          } else {
            toastr.info(res.data.msg, 'Atención');
            return $q.resolve(res.data.rows);
          }
        })
        .catch(function(err) {
          toastr.error(err, 'Error');
          return $q.reject(err);
        });
    };

  });
