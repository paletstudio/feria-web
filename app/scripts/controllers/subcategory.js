'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:SubcategoryCtrl
 * @description
 * # SubcategoryCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.subcategory', {
                url: 'subcategory',
                controller: 'SubcategoryCtrl as vm',
                templateUrl: 'views/subcategory.html',
                loginRequired: true,
                cache: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Subcategorías'
                },
                resolve: {
                    _subcategory: function(subcategory){
                        return subcategory.getAll(true);
                    }
                }
            });
    })
    .controller('SubcategoryCtrl', function(subcategory, $state, _subcategory) {
        var vm = this;
        vm.tituloPagina = $state.current.data.title;
        vm.reload = getSubcategories;
        vm.subcategories = _subcategory;

        function getSubcategories() {
            return subcategory.getAll(true)
                .then(function(res) {
                    vm.subcategories = res;
                });
        }

    });
