(function() {
    'use strict';

    angular
        .module('feriaApp')
        .directive('formFieldSelect', formFieldSelect);

    /* @ngInject */
    function formFieldSelect() {
        var template = [
            '<div class="form-group">',
            '<label class="col-md-3 control-label">{{vm.label}}</label>',
            '<div class="col-md-9">',
            '<ui-select ng-model="vm.innerModel" theme="bootstrap">',
            '<ui-select-match placeholder="{{vm.placeholder}}">{{(vm.catalog === \'ruta\' ? $select.selected.name + \' \' + $select.selected.description : $select.selected.name)}}</ui-select-match>',
            '<ui-select-choices repeat="item in vm.rows | filter: $select.search">',
            '<div ng-bind-html="(vm.catalog === \'ruta\' ? item.name + \' \' + item.description : item.name) | highlight: $select.search"></div>',
            '</ui-select-choices>',
            '</ui-select>',
            '</div>',
            '</div>'
        ].join('');

        var directive = {
            restrict: 'E',
            template: template,
            scope: {
                name: '@',
                ngModel: '=ngModel',
                formStyle: '@',
                label: '@',
                placeholder: '@',
                ngRequired: '=?',
                ngShow: '=?',
                ngHide: '=?',
                ngIf: '=?',
                ngDisabled: '=?',
                ngTrim: '=?',
                pattern: '@',
                ngChange: '&',
                type: '=?',
                tabIndex: '@',
                catalog: '@',
                skipAuthorization: '=?',
                categoryId: '=?'
            },
            require: ['^form', 'ngModel'],
            link: linkFunc,
            controller: FormFieldSelectController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        function linkFunc(scope, el, attr, ctrl) {
            scope.form = ctrl[0];
            scope.horizontal = true;
            scope.placeholder = scope.label;
            scope.inputType = 'text';

            scope.$watch('vm.innerModel', function(newValue) {
                if (newValue) {
                    scope.vm.ngModel = {
                        value: scope.vm.catalog === 'ruta' ? newValue.name + ' ' + newValue.description : newValue.name,
                        value_id: newValue.id
                    };

                    if (scope.ngChange) {
                        scope.ngChange();
                    }
                }
            });

            attr.$observe('formStyle', function(value) {
                if (value === 'vertical') {
                    scope.horizontal = false;
                }
            });

            attr.$observe('placeholder', function(value) {
                scope.placeholder = value;
            });

            attr.$observe('type', function(value) {
                if (value === 'password') {
                    scope.inputType = 'password';
                }
            });
        }
    }

    FormFieldSelectController.$inject = ['API'];

    /* @ngInject */
    function FormFieldSelectController(API) {
        var vm = this;

        activate();

        function activate() {
            getCatalog();
        }

        function getCatalog() {
            if (vm.skipAuthorization) vm.catalog = 'public/' + vm.catalog
            API.get(vm.catalog)
                .then(function(res) {

                    console.log(vm.categoryId)
                    vm.rows = res.data.rows.filter(function(row) {
                        return (vm.categoryId === 2 || vm.categoryId === 3) ? vm.categoryId == row.category_id : true
                    });
                });
        }
    }
})();
