'use strict';

/**
 * @ngdoc filter
 * @name feriaApp.filter:trustHtml
 * @function
 * @description
 * # trustHtml
 * Filter in the feriaApp.
 */
angular.module('feriaApp')
    .filter('trustHtml', ['$sce', function($sce) {
        return function(htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        };
    }]);
