'use strict';

describe('Controller: RutaCtrl', function () {

  // load the controller's module
  beforeEach(module('feriaApp'));

  var RutaCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RutaCtrl = $controller('RutaCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
