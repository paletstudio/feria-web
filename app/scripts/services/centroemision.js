'use strict';

/**
 * @ngdoc service
 * @name feriaApp.centroEmision
 * @description
 * # centroEmision
 * Service in the feriaApp.
 */
angular.module('feriaApp')
    .service('centroEmision', function(API, $q, toastr) {
        var me = this;
        var url = 'centroemision';

        me.init = function() {
            return {
                id: 0,
                name: null,
                address: null,
                opening_hours: null,
                active: 1
            };
        };

        me.getAll = function() {
            return API.get(url)
                .then(function(res) {
                    if (res.data.rows && res.data.rows.length > 0) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.getAllPublic = function() {
            var prefix = 'public/';
            return API.get(prefix + 'centroemision', {
                    skipAuthorization: true
                })
                .then(function(res) {
                    if (res.data.rows && res.data.rows.length > 0) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.get = function(id) {
            return API.get(url + '/' + id)
                .then(function(res) {
                    if (res.data.rows && res.data.rows[0]) {
                        return $q.resolve(res.data.rows[0]);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(null);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.create = function(centroEmision) {
            return API.post(url, centroEmision);
        };

        me.update = function(centroEmision) {
            return API.put(url + '/' + centroEmision.id, centroEmision);
        };

        me.delete = function(centroEmision) {
            return API.delete(url + '/' + centroEmision.id);
        };

        me.save = function(centroEmision) {
            if (centroEmision.id) {
                return me.update(centroEmision);
            } else {
                return me.create(centroEmision);
            }
        };

    });
