'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:EstacionformCtrl
 * @description
 * # EstacionformCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.estacion-new', {
                url: 'estacion/new',
                controller: 'EstacionFormCtrl as vm',
                templateUrl: 'views/estacion.form.html',
                loginRequired: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Estación > Nueva'
                }
            })
            .state('app.estacion-edit', {
                url: 'estacion/{id:int}',
                controller: 'EstacionFormCtrl as vm',
                templateUrl: 'views/estacion.form.html',
                loginRequired: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Estación > Editar'
                }
            });
    })
    .controller('EstacionFormCtrl', function(estacion, $state, toastr, category, sweetAlert) {
        var vm = this;
        var id = $state.params.id;
        vm.tituloPagina = $state.current.data.title;
        vm.estacion = estacion.init();

        function getEstacion(id) {
            return estacion.get(id)
                .then(function(res) {
                    if (res) {
                        vm.estacion = res;
                    } else {
                        $state.go('app.estacion-new');
                    }
                });
        }

        function getCategories() {
            return category.getAllByRoute()
                .then(function(res) {
                    vm.categories = res;
                });
        }

        if (id) {
            getEstacion(id);
        }

        getCategories();

        vm.save = function() {
            estacion.save(vm.estacion)
                .then(function(res) {
                    toastr.success(res.data.msg, 'Exito');
                    if (!vm.estacion.id) vm.clean();
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                });
        };

        vm.clean = function() {
            vm.estacion = estacion.init();
        };

        vm.delete = function() {
            return sweetAlert.confirm('¿Está seguro?')
                .then(function() {
                    return estacion.delete(vm.estacion)
                        .then(function(res) {
                            $state.go('app.estacion');
                            toastr.success(res.data.msg, 'Éxito');
                        });
                });
        };

    });
