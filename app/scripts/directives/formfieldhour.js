'use strict';

/**
 * @ngdoc directive
 * @name sytevApp.directive:formFieldHour
 * @description
 * # formFieldHour
 */
angular.module('feriaApp')
    .directive('formFieldHour', function() {
        var template = [
            '<div class="form-group" ng-class="{\'has-error\': form.{{name}}.$invalid, \'has-success\': form.{{name}}.$valid}">',
                '<label style="margin-top: 30px" ng-class="{\'col-md-3\' : horizontal, \'control-label\': true}">{{label}}</label>',
                '<div ng-class="{\'col-md-9\' : horizontal}">',
                    '<div uib-timepicker ng-model="innerModel" hour-step="\'1\'" show-meridian="showMeridian" minute-step="\'1\'"></div>',
                '</div>',
            '</div>'].join('');

        return {
            template: template,
            restrict: 'E',
            scope: {
                name: '@',
                ngModel: '=ngModel',
                formStyle: '@',
                label: '@',
                placeholder: '@',
                ngRequired: '=?',
                ngShow: '=?',
                ngHide: '=?',
                ngIf: '=?',
                ngDisabled: '=?',
                ngTrim: '=?',
                pattern: '@',
                ngChange: '&',
                type: '=?',
                tabIndex: '@',
            },
            require: ['^form', 'ngModel'],
            link: function postLink(scope, element, attrs, controllers) {
                scope.form = controllers[0];
                scope.horizontal = true;
                scope.placeholder = scope.label;
                scope.inputType = 'text';

                scope.internalChange = function() {
                    scope.ngModel = moment(scope.ngModel).format('YYYY-MM-DD');
                };

                scope.$watch('innerModel', function(newValue) {
                    if (newValue) {
                        scope.ngModel = moment(newValue).format('HH:mm:ss');
                        scope.form[scope.name].$setValidity('', true);
                        if (scope.ngChange) {
                            scope.ngChange();
                        }
                    } else {
                        scope.form[scope.name].$setValidity('', false);
                    }
                });

                attrs.$observe('formStyle', function(value) {
                    if (value === 'vertical') {
                        scope.horizontal = false;
                    }
                });

                attrs.$observe('showMeridian', function(value) {
                    if (value === null) {
                        scope.showMeridian = false;
                    }
                });

                attrs.$observe('placeholder', function(value) {
                    scope.placeholder = value;
                });

                attrs.$observe('type', function(value){
                    if(value === 'password'){
                        scope.inputType = 'password';
                    }
                });
            }
        };
    });
