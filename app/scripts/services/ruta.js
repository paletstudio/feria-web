'use strict';

/**
 * @ngdoc service
 * @name feriaApp.ruta
 * @description
 * # ruta
 * Service in the feriaApp.
 */
angular.module('feriaApp')
    .service('ruta', function(API, $q, toastr) {
        var me = this;
        var url = 'ruta';

        me.init = function() {
            return {
                id: 0,
                name: null,
                description: null,
                active: 1
            };
        };

        me.getAll = function() {
            return API.get(url)
                .then(function(res) {
                    if (res.data.rows && res.data.rows.length > 0) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.getAllPublic = function() {
            return API.get('public/ruta', {
                    skipAuthorization: true
                })
                .then(function(res) {
                    if (res.data.rows && res.data.rows.length > 0) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.warning(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.get = function(id) {
            return API.get(url + '/' + id)
                .then(function(res) {
                    if (res.data.rows && res.data.rows[0]) {
                        return $q.resolve(res.data.rows[0]);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(null);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.create = function(ruta) {
            return API.post(url, ruta);
        };

        me.update = function(ruta) {
            return API.put(url + '/' + ruta.id, ruta);
        };

        me.delete = function(ruta) {
            return API.delete(url + '/' + ruta.id);
        };

        me.save = function(ruta) {
            if (ruta.id) {
                return me.update(ruta);
            } else {
                return me.create(ruta);
            }
        };

    });
