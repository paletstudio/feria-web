'use strict';

/**
 * @ngdoc service
 * @name feriaApp.oxxo
 * @description
 * # oxxo
 * Service in the feriaApp.
 */
angular.module('feriaApp')
    .service('oxxo', function(API, $q, toastr) {
        var me = this;
        var url = 'oxxo';

        me.init = function() {
            return {
                id: 0,
                name: null,
                description: null,
                active: 1
            };
        };

        me.getAll = function() {
            return API.get(url)
                .then(function(res) {
                    if(res.data.rows && res.data.rows.length > 0){
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.getAllPublic = function() {
            var prefix = 'public/';
            return API.get(prefix + 'oxxo', {
                    skipAuthorization: true
                })
                .then(function(res) {
                    if (res.data.rows && res.data.rows.length > 0) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.get = function(id) {
            return API.get(url + '/' + id)
                .then(function(res) {
                    if (res.data.rows && res.data.rows[0]) {
                        return $q.resolve(res.data.rows[0]);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(null);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.create = function(oxxo) {
            return API.post(url, oxxo);
        };

        me.update = function(oxxo) {
            return API.put(url + '/' + oxxo.id, oxxo);
        };

        me.delete = function(oxxo) {
            return API.delete(url + '/' + oxxo.id);
        };

        me.save = function(oxxo) {
            if (oxxo.id) {
                return me.update(oxxo);
            } else {
                return me.create(oxxo);
            }
        };

    });
