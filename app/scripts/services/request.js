'use strict';

/**
 * @ngdoc service
 * @name feriaApp.request
 * @description
 * # request
 * Service in the feriaApp.
 */
angular.module('feriaApp')
    .service('request', function(API, $uibModal, $q, toastr, settings, utilsService) {
        var me = this;
        var url = 'request';

        me.init = function() {
            return {
                id: 0,
                applicant_name: null,
                applicant_family_name: null,
                applicant_family_name_2: null,
                applicant_phone: null,
                applicant_email: null,
                //applicant_identifier: null,
                // subject: null,
                // description: null,
                // event_date: null,
                // application_datetime: null,
                subcategory_id: 0,
                // subcategory_value_1: null,
                // subcategory_value_2: null,
                status_id: 0,
                user_id: 0,
                requestorigin_id: 0,
                response: 0,
                active: 1,
                data: []
            };
        };

        me.getAll = function(params) {
            var endpoint = url;
            if (params) {
                if (params.info) endpoint = utilsService.updateQueryStringParameter(endpoint, 'info', true);
                if (params.type) endpoint = utilsService.updateQueryStringParameter(endpoint, 'type', params.type);
                if (params.paginate) endpoint = utilsService.updateQueryStringParameter(endpoint, 'paginate', true);
                if (params.page) endpoint = utilsService.updateQueryStringParameter(endpoint, 'page', params.page);
                if (params.itemsPerPage) endpoint = utilsService.updateQueryStringParameter(endpoint, 'items', params.itemsPerPage);
                if (params.status) endpoint = utilsService.updateQueryStringParameter(endpoint, 'status', params.status);
            }

            return API.get(endpoint)
                .then(function(res) {
                    if (res.data.rows && (res.data.rows.length > 0 || res.data.rows.data.length > 0)) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.getRequestTypes = function() {
            return API.get(url + '/type')
                .then(function(res) {
                    if (res.data.rows && res.data.rows.length > 0) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.get = function(id) {
            return API.get(url + '/' + id)
                .then(function(res) {
                    if (res.data.rows) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(null);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.getCountAll = function() {
            return API.get(url + '/count')
                .then(function(res) {
                    return $q.resolve(res.data.rows);
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.getUnrespondedCount = function() {
            return API.get(url + '/count?status_id=3')
                .then(function(res) {
                    return $q.resolve(res.data.rows);
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.getByUserCount = function() {
            return API.get(url + '/user/count')
                .then(function(res) {
                    return $q.resolve(res.data.rows);
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.create = function(request) {
            return API.post(url, request);
        };

        me.update = function(request) {
            return API.put(url + '/' + request.id, request);
        };

        me.delete = function(request) {
            return API.delete(url + '/' + request.id);
        };

        me.save = function(request) {
            if (request.id) {
                return me.update(request);
            } else {
                return me.create(request);
            }
        };

        me.getMessages = function(idRequest) {
            return API.get(url + '/' + idRequest + '/message')
                .then(function(res) {
                    return $q.resolve(res.data.rows);
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.saveMessage = function(message) {
            return API.post(url + '/message', message)
                .then(function(res) {
                    return res.data;
                });
        };

        me.getRequestUser = function(params) {
            var endpoint = url + '/from/user';
            if (params) {
                if (params.type) endpoint = utilsService.updateQueryStringParameter(endpoint, 'type', params.type);
                if (params.paginate) endpoint = utilsService.updateQueryStringParameter(endpoint, 'paginate', true);
                if (params.page) endpoint = utilsService.updateQueryStringParameter(endpoint, 'page', params.page);
                if (params.itemsPerPage) endpoint = utilsService.updateQueryStringParameter(endpoint, 'items', params.itemsPerPage);
                if (params.status) endpoint = utilsService.updateQueryStringParameter(endpoint, 'status', params.status);
            }

            return API.get(endpoint)
                .then(function(res) {
                    if (res.data.rows && (res.data.rows.length > 0 || res.data.rows.data.length > 0)) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.delegateRequest = function(request, users) {
            var instance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/template/delegaterequest.html',
                controller: 'DelegateRequestCtrl',
                controllerAs: 'vm',
                size: 'lg',
                resolve: {
                    request: function() {
                        return request;
                    },
                    users: function() {
                        return users;
                    }
                }
            });

            return instance.result.then(function() {

            });
        };

        me.resolveRequest = function(request) {
            var instance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/template/resolverequest.html',
                controller: 'ResolveRequestCtrl',
                controllerAs: 'vm',
                size: 'lg',
                resolve: {
                    request: function() {
                        return request;
                    }
                }
            });

            return instance.result.then(function() {

            });
        };

        me.respondRequest = function(request) {
            return me.checkRequestStatus(request.id, 3)
                .then(function(res) {
                    if (!res) {
                        toastr.warning('Esta queja/sugerencia esta siendo/ha sido respondida.');
                        return $q.reject();
                    }
                    return me.patch(request.id, {
                        status_id: 4
                    });
                })
                .then(function() {
                    return $uibModal.open({
                        animation: true,
                        ariaLabelledBy: 'modal-title',
                        ariaDescribedBy: 'modal-body',
                        templateUrl: 'views/template/respondrequest.html',
                        controller: 'RespondRequestCtrl',
                        controllerAs: 'vm',
                        size: 'lg',
                        resolve: {
                            _request: function() {
                                return request;
                            }
                        }
                    }).result;
                });
        };

        me.patch = function(id, data) {
            return API.patch(url + '/' + id, data)
                .then(function(res) {
                    return $q.resolve(res.data.rows);
                });
        };

        me.checkRequestStatus = function(id, status) {
            var path = url + '/' + id + '/status';
            if (status) {
                path += '?status_id=' + status;
            }
            return API.get(path)
                .then(function(res) {
                    return $q.resolve(res.data.rows);
                });
        };

        me.displayImage = function(uniqueid) {
            return settings.baseUrl + 'public/image/' + uniqueid;
        };

        me.showTooltip = function(request) {
            var tooltip = '';
            var label = '';
            var property;
            if (request.status.id === 2) {
                label = 'Asignada a: ';
                property = 'assigned';
            } else if (request.status.id === 3) {
                label = 'Resuelta por: ';
                property = 'resolved';
            } else if (request.status.id === 4) {
                label = 'Respondida por: ';
                property = 'responded';
            }
            if (property && request[property] && request[property].user) {
                tooltip = label + request[property].user.name + ' ' + request[property].user.family_name;
            }
            return tooltip;
        };

        me.requestUserInfo = function(request) {
            return ['<p class="request-user-info"><b>Nombre: </b>' + request.applicant_name + '</p>',
                '<p class="request-user-info"><b>Apellido Paterno: </b> ' + request.applicant_family_name + ' </p>',
                '<p class="request-user-info"><b>Apellido Materno: </b> ' + request.applicant_family_name_2 + ' </p>',
                '<p class="request-user-info"><b>No. tarjeta: </b>' + request.applicant_identifier + '</p>',
                '<p><b><i class="fa fa-phone" aria-hidden="true"></i> </b>' + request.applicant_phone + '</p>',
                '<p><b><i class="fa fa-envelope" aria-hidden="true"></i> </b>' + request.applicant_email + '</p>'
            ].join('');
        };

        me.getIdentifierOptions = function() {
            return [{
                id: 0,
                name: 'Tarjeta',
                pattern: '\\d{10}',
                placeholder: 'Número de tarjeta (10 dígitos)'
            }, {
                id: 1,
                name: 'Folio',
                pattern: '\([a-zA-Z]{1}\\d{7}|(a|A)\\d{6})',
                placeholder: 'Folio de la tarjeta (1 letra y 7 dígitos)'
            }];
        };

    });
