'use strict';

/**
 * @ngdoc service
 * @name feriaApp.Indicator
 * @description
 * # Indicator
 * Service in the feriaApp.
 */
angular.module('feriaApp')
    .service('indicator', function(API, $q, toastr, utilsService) {
        var me = this;
        var url = 'indicator';

        me.requestStatus = function() {
            return API.get(url + '/requestStatus')
                .then(function(res) {
                    if (res.data.rows) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(null);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.category = function(params) {
            var endpoint = url + '/category';
            if (params) {
                if (params.type) endpoint = utilsService.updateQueryStringParameter(endpoint, 'type', params.type);
            }

            return API.get(endpoint)
                .then(function(res) {
                    if (res.data.rows) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(null);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.categoryAverage = function() {
            return API.get(url + '/category/average')
                .then(function(res) {
                    if (res.data.rows) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(null);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.dataToGraphic = function(data, labelProperty, dataProperty) {
            var obj = {
                labels: [],
                data: [],
                total: 0
            };

            data.data.forEach(function(d) {
                obj.labels.push(d[labelProperty]);
                obj.data.push(d[dataProperty]);
            });

            obj.total = data.total;

            return obj;
        };

    });
