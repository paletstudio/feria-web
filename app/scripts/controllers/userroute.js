'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:UserrouteCtrl
 * @description
 * # UserrouteCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.user-ruta', {
                url: 'user/ruta',
                controller: 'UserRouteCtrl as vm',
                templateUrl: 'views/userroute.html',
                loginRequired: true,
                blacklist: [
                    'auditor', 'admin'
                ],
                data: {
                    title: 'Rutas del usuario'
                }
            });
    });

angular.module('feriaApp')
    .controller('UserRouteCtrl', function(user, $state, session) {
        var vm = this;
        vm.tituloPagina = $state.current.data.title;
        vm.reload = getRutas;

        activate();

        function activate() {
            getRutas();
            getLoggedinUser();
        }

        function getRutas() {
            user.getRutas()
                .then(function(res) {
                    console.log(res);
                    vm.rutas = res;
                });
        }

        function getLoggedinUser() {
            vm.user = session.getLoggedinUser();
        }
    });
