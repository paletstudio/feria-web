'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:UserCtrl
 * @description
 * # UserCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.user', {
                url: 'user',
                controller: 'UserCtrl as vm',
                templateUrl: 'views/user.html',
                loginRequired: true,
                cache: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Usuarios'
                },
                resolve: {
                    _user: function(user){
                        return user.getAll(true);
                    }
                }
            });
    })
    .controller('UserCtrl', function($state, user, _user) {
        var vm = this;
        vm.tituloPagina = $state.current.data.title;
        vm.reload = getUsers;
        vm.users = _user;

        function getUsers() {
            return user.getAll(true)
                .then(function(res) {
                    vm.users = res;
                });
        }

    });
