'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:UserPasswordCtrl
 * @description
 * # UserPasswordCtrl
 * Controller of the feriaApp
 */


config.$inject = ['$stateProvider'];

function config($stateProvider) {
    $stateProvider
        .state('app.user-password', {
            url: 'user/password',
            controller: 'UserPasswordCtrl as vm',
            templateUrl: 'views/user.password.html',
            loginRequired: true,
            blacklist: [],
            data: {
                title: 'Usuario > Cambio de contraseña'
            }
        });
}

UserPasswordCtrl.$inject = ['$state', 'user', 'toastr'];

function UserPasswordCtrl($state, user, toastr) {
    var vm = this;
    vm.pageTitle = $state.current.data.title;
    vm.clean = clean;
    vm.save = save;
    vm.user = {};
    vm.disableChangePassword = disableChangePassword;

    function save() {
        user.changePassword(vm.user)
            .then(function(res) {
                toastr.success(res.msg);
                clean();
            })
            .catch(function(err) {
                toastr.error(err);
            });
    }

    function clean() {
        vm.user = {};
    }

    function disableChangePassword() {
        return !vm.user.oldPassword || !vm.user.newPassword || !vm.user.reNewPassword || vm.user.oldPassword === vm.user.newPassword || vm.user.newPassword !== vm.user.reNewPassword;
    }
}

angular.module('feriaApp')
    .config(config)
    .controller('UserPasswordCtrl', UserPasswordCtrl);
