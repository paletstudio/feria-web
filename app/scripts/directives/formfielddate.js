'use strict';

/**
 * @ngdoc directive
 * @name sytevApp.directive:formFieldDate
 * @description
 * # formFieldDate
 */
angular.module('feriaApp')
    .directive('formFieldDate', function() {
        var template = [
            '<div class="form-group" ng-class="{\'has-error\': form.{{name}}.$invalid, \'has-success\': form.{{name}}.$valid}">',
                '<label ng-class="{\'col-md-3\' : horizontal, \'control-label\': true}">{{label}}</label>',
                '<div ng-class="{\'col-md-9\' : horizontal}">',
                    '<div class="input-group">',
                        '<input type="text" uib-datepicker-popup ng-model="innerModel" ng-change="internalChange()" datepicker-options="{minDate: minDate, maxDate: maxDate, showWeeks: false}" ng-click="dpOpened = !dpOpened" is-open="dpOpened" show-button-bar="false" ng-required="ngRequired" name="{{name}}" placeholder="{{placeholder}}" class="form-control">',
                        '<span class="input-group-btn">',
                            '<button class="btn btn-link" type="button" ng-click="dpOpened = !dpOpened">',
                                '<i class="fa fa-calendar"></i>',
                            '</button>',
                        '</span>',
                    '</div>',
                '</div>',
            '</div>'].join('');

        return {
            template: template,
            restrict: 'E',
            scope: {
                name: '@',
                ngModel: '=ngModel',
                formStyle: '@',
                label: '@',
                placeholder: '@',
                ngRequired: '=?',
                ngShow: '=?',
                ngHide: '=?',
                ngIf: '=?',
                ngDisabled: '=?',
                ngTrim: '=?',
                pattern: '@',
                ngChange: '&',
                type: '=?',
                tabIndex: '@',
                maxDate: '=?',
                minDate: '=?'
            },
            require: ['^form', 'ngModel'],
            link: function postLink(scope, element, attrs, controllers) {
                scope.form = controllers[0];
                scope.horizontal = true;
                scope.placeholder = scope.label;
                scope.inputType = 'text';

                scope.internalChange = function() {
                    scope.ngModel = moment(scope.ngModel).format('YYYY-MM-DD');
                };

                scope.$watch('innerModel', function(newValue) {
                    scope.ngModel = moment(newValue).format('YYYY-MM-DD');
                    if (scope.ngChange) {
                        scope.ngChange();
                    }
                });

                attrs.$observe('formStyle', function(value) {
                    if (value === 'vertical') {
                        scope.horizontal = false;
                    }
                });

                attrs.$observe('placeholder', function(value) {
                    scope.placeholder = value;
                });

                attrs.$observe('type', function(value){
                    if(value === 'password'){
                        scope.inputType = 'password';
                    }
                });
            }
        };
    });
