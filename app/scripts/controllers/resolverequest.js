'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:ResolverequestCtrl
 * @description
 * # ResolverequestCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .controller('ResolveRequestCtrl', function($scope, $uibModalInstance, request, API, toastr, locker) {
        var vm = this;
        var url = 'request';
        vm.request = angular.copy(request);
        vm.cancel = cancel;
        vm.save = save;

        function cancel() {
            $uibModalInstance.dismiss(null);
        }

        function save(request, status) {
            vm.request.status_id = status;
            vm.request.resolve_user_id = locker.get('user').id;
            var req = angular.copy(vm.request);
            delete req.status;
            delete req.subcategory;
            delete req.origin;

            return API.put(url + '/' + vm.request.id, req)
                .then(function(res) {
                    $scope.$emit('request.resolve');
                    $uibModalInstance.close();
                    toastr.success(res.data.msg, 'Éxito');
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                });
        }

    });
