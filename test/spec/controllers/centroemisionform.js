'use strict';

describe('Controller: CentroemisionformCtrl', function () {

  // load the controller's module
  beforeEach(module('feriaApp'));

  var CentroemisionformCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CentroemisionformCtrl = $controller('CentroemisionformCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
