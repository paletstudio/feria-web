'use strict';
angular.module('feriaApp')
    .service('auditor', function(API, $q, user) {
        var me = this;
        var url = 'user/auditor';

        me.getAll = function() {
            return user.getAuditors();
        };

        me.assignEmployees = function(auditor, employees) {
            return API.post(url, {auditor: auditor, employees: employees})
            .then(function(res) {
                return $q.resolve(res.data);
            })
            .catch(function(err) {
                return $q.reject(err);
            })
        };
    });
