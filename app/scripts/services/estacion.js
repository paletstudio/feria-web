'use strict';

/**
 * @ngdoc service
 * @name feriaApp.estacion
 * @description
 * # estacion
 * Service in the feriaApp.
 */
angular.module('feriaApp')
    .service('estacion', function(API, $q, toastr) {
        var me = this;
        var url = 'estacion';

        me.init = function() {
            return {
                id: 0,
                name: null,
                description: null,
                active: 1
            };
        };

        me.getAll = function() {
            return API.get(url)
                .then(function(res) {
                    if (res.data.rows && res.data.rows.length > 0) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.getAllPublic = function() {
            var prefix = 'public/';
            return API.get(prefix + 'estacion', {
                    skipAuthorization: true
                })
                .then(function(res) {
                    if (res.data.rows && res.data.rows.length > 0) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.get = function(id) {
            return API.get(url + '/' + id)
                .then(function(res) {
                    if (res.data.rows && res.data.rows[0]) {
                        return $q.resolve(res.data.rows[0]);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(null);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.create = function(estacion) {
            return API.post(url, estacion);
        };

        me.update = function(estacion) {
            return API.put(url + '/' + estacion.id, estacion);
        };

        me.delete = function(estacion) {
            return API.delete(url + '/' + estacion.id);
        };

        me.save = function(estacion) {
            if (estacion.id) {
                return me.update(estacion);
            } else {
                return me.create(estacion);
            }
        };

    });
