'use strict';

describe('Controller: RequestformCtrl', function () {

  // load the controller's module
  beforeEach(module('feriaApp'));

  var RequestformCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RequestformCtrl = $controller('RequestformCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(RequestformCtrl.awesomeThings.length).toBe(3);
  });
});
