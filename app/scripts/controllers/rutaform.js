'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:RutaformCtrl
 * @description
 * # RutaformCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.ruta-new', {
                url: 'ruta/new',
                controller: 'RutaFormCtrl as vm',
                templateUrl: 'views/ruta.form.html',
                loginRequired: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Ruta > Nueva'
                }
            })
            .state('app.ruta-edit', {
                url: 'ruta/{id:int}',
                controller: 'RutaFormCtrl as vm',
                templateUrl: 'views/ruta.form.html',
                loginRequired: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Ruta > Editar'
                }
            });
    })
    .controller('RutaFormCtrl', function(ruta, $state, toastr, category, sweetAlert, user) {
        var vm = this;
        var id = $state.params.id;
        vm.tituloPagina = $state.current.data.title;
        vm.ruta = ruta.init();

        function getRuta(id) {
            return ruta.get(id)
                .then(function(res) {
                    if (res) {
                        vm.ruta = res;
                    } else {
                        $state.go('app.ruta-new');
                    }
                });
        }

        function getCategories() {
            return category.getAllByRoute()
                .then(function(res) {
                    vm.categories = res;
                });
        }

        function getMods() {
            user.getMods()
                .then(function(res) {
                	vm.users = res;
                });
        }

        if (id) {
            getRuta(id);
        }

        getCategories();
        getMods();

        vm.save = function() {
            ruta.save(vm.ruta)
                .then(function(res) {
                    toastr.success(res.data.msg, 'Exito');
                    if (!vm.ruta.id) vm.clean();
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                });
        };

        vm.clean = function() {
            vm.ruta = ruta.init();
        };

        vm.delete = function() {
            return sweetAlert.confirm('¿Está seguro?')
                .then(function() {
                    return ruta.delete(vm.ruta)
                        .then(function(res) {
                            $state.go('app.ruta');
                            toastr.success(res.data.msg, 'Éxito');
                        });
                });
        };

    });
