(function() {
    'use strict';

    angular
        .module('feriaApp')
        .service('utilsService', utilsService);

    utilsService.$inject = [];

    /* @ngInject */
    function utilsService() {

        this.updateQueryStringParameter = function(uri, key, value) {
            var re = new RegExp('([?&])' + key + '=.*?(&|$)', 'i');
            var separator = uri.indexOf('?') !== -1 ? '&' : '?';
            if (uri.match(re)) {
                return uri.replace(re, '$1' + key + '=' + value + '$2');
            } else {
                return uri + separator + key + '=' + value;
            }
        };
    }
})();
