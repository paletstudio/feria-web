'use strict';

describe('Service: ruta', function () {

  // load the service's module
  beforeEach(module('feriaApp'));

  // instantiate service
  var ruta;
  beforeEach(inject(function (_ruta_) {
    ruta = _ruta_;
  }));

  it('should do something', function () {
    expect(!!ruta).toBe(true);
  });

});
