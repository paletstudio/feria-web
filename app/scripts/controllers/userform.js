'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:UserformCtrl
 * @description
 * # UserformCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.user-new', {
                url: 'user/new',
                controller: 'UserFormCtrl as vm',
                templateUrl: 'views/user.form.html',
                loginRequired: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Usuario > Nuevo'
                }
            })
            .state('app.user-edit', {
                url: 'user/{id:int}',
                controller: 'UserFormCtrl as vm',
                templateUrl: 'views/user.form.html',
                loginRequired: true,
                cache: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Usuario > Editar'
                }
            });
    })
    .controller('UserFormCtrl', function($state, user, toastr, workingplace, profile, jobposition, requestorigin,locker, sweetAlert) {
        var vm = this;
        var id = $state.params.id;
        vm.tituloPagina = $state.current.data.title;

        function getUser(id) {
            return user.get(id)
                .then(function(res) {
                    if (res) {
                        vm.user = res;
                    } else {
                        $state.go('app.user-new');
                    }
                });
        }

        function getWorkingplaces() {
            workingplace.getAll()
                .then(function(res) {
                    vm.workingplaces = res;
                });
        }

        function getProfiles() {
            profile.getAll()
                .then(function(res) {
                    vm.profiles = res;
                });
        }

        function getJobpositions() {
            jobposition.getAll()
                .then(function(res) {
                    vm.jobpositions = res;
                });
        }

        function getOrigins(){
            requestorigin.getVisibles()
            .then(function(res){
                vm.origins = res;
            });
        }

        getJobpositions();
        getWorkingplaces();
        getProfiles();
        getOrigins();

        if (id) {
            getUser(id);
        } else {
            vm.user = user.init();
        }

        function reloadStorage(){
            var user = locker.get('user');
            if(id === user.id){
                locker.forget(['user']);
                user.requestorigin = vm.user.requestorigin_id;
                locker.put('user', user);
            }
        }

        vm.save = function() {
            user.save(vm.user)
                .then(function(res) {
                    toastr.success(res.data.msg, 'Exito');
                    reloadStorage();
                    if (!vm.user.id) vm.clean();
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                });
        };

        vm.clean = function() {
            vm.user = user.init();
        };

        vm.delete = function() {
            sweetAlert.confirm('Esta acción es irreversible', '¿Estás seguro?')
                .then(function() {
                    return user.delete(vm.user.id)
                })
                .then(function(res) {
                    toastr.success(res.data.msg, 'Éxito');
                    $state.go('app.user');
                });
        };

    });
