'use strict';

describe('Directive: isInRole', function () {

  // load the directive's module
  beforeEach(module('feriaApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<is-in-role></is-in-role>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the isInRole directive');
  }));
});
