'use strict';

/**
 * @ngdoc service
 * @name feriaApp.workingplace
 * @description
 * # workingplace
 * Service in the feriaApp.
 */
angular.module('feriaApp')
    .service('workingplace', function(API, $q, toastr) {

        var me = this;
        var url = 'workingplace';

        /*me.init = function() {
            return {
                id: 0,
                profile_id: 0,
                name: null,
                family_name: null,
                family_name_2: null,
                username: null,
                email: null,
                password: null,
                workingplace_id:0,
                jobposition_id:0,
                active: 1
            };
        };*/

        me.getAll = function() {
            return API.get(url)
                .then(function(res) {
                    if (res.data.rows && res.data.rows.length > 0) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.get = function(id) {
            return API.get(url + '/' + id)
                .then(function(res) {
                    if (res.data.rows && res.data.rows[0]) {
                        return $q.resolve(res.data.rows[0]);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(null);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.create = function(workingplace) {
            return API.post(url, workingplace);
        };

        me.update = function(workingplace) {
            return API.put(url + '/' + workingplace.id, workingplace);
        };

        me.save = function(workingplace) {
            if (workingplace.id) {
                return me.update(workingplace);
            } else {
                return me.create(workingplace);
            }
        };

    });
