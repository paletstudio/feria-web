'use strict';

describe('Service: centroEmision', function () {

  // load the service's module
  beforeEach(module('feriaApp'));

  // instantiate service
  var centroEmision;
  beforeEach(inject(function (_centroEmision_) {
    centroEmision = _centroEmision_;
  }));

  it('should do something', function () {
    expect(!!centroEmision).toBe(true);
  });

});
