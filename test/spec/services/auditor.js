'use strict';

describe('Service: auditor', function () {

  // load the service's module
  beforeEach(module('feriaApp'));

  // instantiate service
  var auditor;
  beforeEach(inject(function (_auditor_) {
    auditor = _auditor_;
  }));

  it('should do something', function () {
    expect(!!auditor).toBe(true);
  });

});
