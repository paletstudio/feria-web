'use strict';

/**
 * @ngdoc directive
 * @name feriaApp.directive:isInRole
 * @description
 * # isInRole
 */
angular.module('feriaApp')
    .directive('isInRole', function(session) {
        return function(scope, element, attrs) {
                attrs.$observe('isInRole', function(value) {
                    var check = session.isInRole(value);

                    if (!check) {
                        element.remove();
                    }
                }, true);
            };
    });
