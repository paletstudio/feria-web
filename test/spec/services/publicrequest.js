'use strict';

describe('Service: publicrequest', function () {

  // load the service's module
  beforeEach(module('feriaApp'));

  // instantiate service
  var publicrequest;
  beforeEach(inject(function (_publicrequest_) {
    publicrequest = _publicrequest_;
  }));

  it('should do something', function () {
    expect(!!publicrequest).toBe(true);
  });

});
