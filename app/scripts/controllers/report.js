'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:ReportCtrl
 * @description
 * # ReportCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.report', {
                url: 'report?employee',
                controller: 'ReportCtrl as vm',
                templateUrl: 'views/reports.html',
                loginRequired: true,
                blacklist: [
                    'user'
                ],
                data: {
                    title: 'Reportes'
                }
            });
    })
    .controller('ReportCtrl', function($state, $q, report, request, status, category, subcategory, requestorigin, session, user, $uibModal) {

        var vm = this;
        vm.tituloPagina = $state.current.data.title;
        if ($state.params.employee) {
            vm.filter.employee = parseInt($state.params.employee);
        }
        // vm.filterSubcategories = filterSubcategories;
        // vm.filter = report.filterInit();
        // vm.generate = generate;
        // vm.clean = clean;
        vm.paginate = paginate;
        vm.exportExcel = exportExcel;
        vm.requestUserInfo = requestUserInfo;
        vm.user = session.getLoggedinUser();
        vm.filter = {
            type: 1,
            paginate: true
        };
        var catalogs = {};

        catalogs.status = [{
            id: '',
            name: 'Todos'
        }];
        catalogs.categories = [{
            id: '',
            name: 'Todos'
        }];
        catalogs.origins = [{
            id: '',
            name: 'Todos'
        }];
        catalogs.employees = [{
            employee_id: '',
            user: {
                name: 'Todos'
            }
        }];

        activate();

        function activate() {
            var promises = {
                status: status.getAll(),
                categories: category.getAll(),
                subcategories: subcategory.getAll(),
                origins: requestorigin.getAll(),
                types: request.getRequestTypes()
            };

            if (session.isInRole('auditor')) {
                promises.employees = user.getEmployees(vm.user.id);
            }

            $q.all(promises)
                .then(function(res) {
                    catalogs.status = catalogs.status.concat(res.status);
                    catalogs.categories = catalogs.categories.concat(res.categories);
                    catalogs.subcategories = res.subcategories;
                    catalogs.origins = catalogs.origins.concat(res.origins);
                    catalogs.types = res.types;
                    if (session.isInRole('auditor')) {
                        catalogs.employees = catalogs.employees.concat(res.employees.rows.employees);
                    }
                });
        }

        function generate() {
            report.generateReport(vm.filter)
                .then(function(res) {
                    vm.report = res.data;
                    vm.totalItems = res.total;
                });
        }

        function clean() {
            vm.filter = report.filterInit();
            vm.report = [];
        }

        function exportExcel(filter) {
            report.exportExcel(filter);
        }

        function requestUserInfo(request) {
            return ['<p class="request-user-info"><b>Nombre: </b>' + request.applicant_name + '</p>',
                '<p class="request-user-info"><b>Apellido Paterno: </b> ' + request.applicant_family_name + ' </p>',
                '<p class="request-user-info"><b>Apellido Materno: </b> ' + request.applicant_family_name_2 + ' </p>',
                '<p class="request-user-info"><b>No. tarjeta: </b>' + request.applicant_identifier + '</p>',
                '<p><b><i class="fa fa-phone" aria-hidden="true"></i>: </b>' + request.applicant_phone + '</p>',
                '<p><b><i class="fa fa-envelope" aria-hidden="true"></i>: </b>' + request.applicant_email + '</p>'
            ].join('');
        }

        function paginate(data) {
            vm.filter.page = data.page;
            vm.filter.items = data.itemsPerPage;
            console.log(data);
            generate();
        }

        vm.openModal = function() {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'myModalContent.html',
                controller: 'FiltrosReporteCtrl',
                controllerAs: 'vm',
                size: 'lg',
                // appendTo: parentElem,
                resolve: {
                    catalogs: function() {
                        return catalogs;
                    },
                    filter: function() {
                        return angular.copy(vm.filter);
                    }
                }
            });

            modalInstance.result
                .then(function(result) {
                    vm.filter = result;
                    generate();
                }, function() {
                    // $log.info('Modal dismissed at: ' + new Date());
                });
        };

    });

angular.module('feriaApp')
    .controller('FiltrosReporteCtrl', function($scope, catalogs, filter, $uibModalInstance) {
        var vm = this;
        console.log(catalogs);
        vm.origins = catalogs.origins;
        vm.subcategories = catalogs.subcategories;
        vm.status = catalogs.status;
        vm.types = catalogs.types;
        vm.employees = catalogs.employees;
        vm.today = moment().toDate();
        vm.filter = filter;

        vm.filterCategories = function() {
            vm.categories = catalogs.categories.filter(function(c) {
                return c.category_type_id === vm.filter.type;
            });
        };

        vm.filterCategories();

        vm.filterSubcategories = function(categoryid) {
            vm.subcategories = catalogs.subcategories.filter(function(s) {
                return s.category_id === categoryid;
            });
            vm.subcategories.unshift({
                id: '',
                name: 'Todos'
            });
        };

        vm.ok = function() {
            $uibModalInstance.close(vm.filter);
        };

        vm.cancel = function() {
            $uibModalInstance.dismiss();
        };
    });
