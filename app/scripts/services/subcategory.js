'use strict';

/**
 * @ngdoc service
 * @name feriaApp.subcategory
 * @description
 * # subcategory
 * Service in the feriaApp.
 */
angular.module('feriaApp')
    .service('subcategory', function(API, $q, toastr) {
        var me = this;
        var url = 'subcategory';

        me.init = function() {
            return {
                id: 0,
                category_id: 0,
                name: null,
                description: null,
                active: 1
            };
        };

        me.getAll = function(info) {
            var query = '';
            if (info) {
                query = '?info=true';
            }
            return API.get(url + query)
                .then(function(res) {
                    if (res.data.rows && res.data.rows.length > 0) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.getAllPublic = function() {
            var prefix = 'public/';
            return API.get(prefix + url, {
                    skipAuthorization: true
                })
                .then(function(res) {
                    if (res.data.rows && res.data.rows.length > 0) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.get = function(id) {
            return API.get(url + '/' + id)
                .then(function(res) {
                    if (res.data.rows && res.data.rows[0]) {
                        return $q.resolve(res.data.rows[0]);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(null);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.create = function(subcategory) {
            return API.post(url, subcategory);
        };

        me.update = function(subcategory) {
            return API.put(url + '/' + subcategory.id, subcategory);
        };

        me.delete = function(subcategory) {
            return API.delete(url + '/' + subcategory.id);
        };

        me.save = function(subcategory) {
            subcategory.category_id = subcategory.category.id;
            if (subcategory.id) {
                return me.update(subcategory);
            } else {
                return me.create(subcategory);
            }
        };

    });
