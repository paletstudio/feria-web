(function() {
    'use strict';

    angular
        .module('feriaApp')
        .directive('header', header);

    /* @ngInject */
    function header() {
        var directive = {
            restrict: 'E',
            templateUrl: 'views/main/header.html',
            scope: {},
            controller: Controller,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;
    }

    Controller.$inject = ['$scope', 'settings', 'locker', 'session', 'socket', 'toastr', 'request', '$state', '$rootScope'];

    /* @ngInject */
    function Controller($scope, settings, locker, session, socket, toastr, request, $state, $rootScope) {
        var vm = this;
        var user = locker.get('user');
        vm.notificationCount = 0;
        vm.nombreApp = settings.nameApp;
        vm.versionApp = settings.version;
        vm.usuario = user ? user.name + ' ' + user.family_name : '';
        vm.logout = session.logout;

        activate();

        function activate() {
            getCountRequest();
            getUserAssignedCountRequests();
            getUnrespondedRequestCount();
        }

        function getUnrespondedRequestCount() {
            return request.getUnrespondedCount()
                .then(function(res) {
                    vm.unresponded = res;
                });
        }

        function getUserAssignedCountRequests() {
            return request.getByUserCount()
                .then(function(res) {
                    vm.myNotificationCount = res;
                });
        }

        function getCountRequest() {
            return request.getCountAll()
                .then(function(res) {
                    vm.unassigned = res;
                });
        }

        socket.on('request.new', function(data) {
            if (session.isInRole('admin')) {
                getCountRequest();
                toastr.info('Queja/sug #' + data.id, 'Nueva notificacion!', {
                    preventDuplicates: true
                });
            }
        });

        socket.on('request.delegate.' + user.id, function(data) {
            getCountRequest();
            getUserAssignedCountRequests();
            getUnrespondedRequestCount();
            toastr.info('Te ha sido asignada la queja #' + data.request_id, 'Nueva notificacion!', {
                preventDuplicates: true,
                onTap: function() {
                    $state.go('app.request-show', {
                        id: data.request_id
                    });
                }
            });
        });

        var requestDelegate = $rootScope.$on('request.delegate', function() {
            getCountRequest();
            getUserAssignedCountRequests();
            getUnrespondedRequestCount();
        });

        var requestResolve = $rootScope.$on('request.resolve', function() {
            getCountRequest();
            getUserAssignedCountRequests();
            getUnrespondedRequestCount();
        });

        vm.$onDestroy = function() {
            socket.removeAllListeners();
            requestDelegate();
            requestResolve();
        };

    }
})();
