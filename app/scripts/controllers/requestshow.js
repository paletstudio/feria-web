'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:RequestshowCtrl
 * @description
 * # RequestshowCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.request-show', {
                url: 'request/{id}/show',
                controller: 'RequestShowCtrl as vm',
                templateUrl: 'views/request.show.html',
                loginRequired: true,
                /*
                               validateAccess: true,
                               cache: false,*/
                data: {
                    title: 'Sugerencia > Detalles'
                }
            });
    })
    .controller('RequestShowCtrl', function(request, $state, toastr, locker, user) {
        var vm = this;
        var id = $state.params.id;
        var users = [];
        vm.tituloPagina = $state.current.data.title;
        vm.messages = [];
        vm.saveMessage = saveMessage;
        vm.resolve = resolveModal;
        vm.displayImage = displayImage;
        vm.delegateRequest = delegateRequest;
        vm.respond = respondRequest;

        if (id) {
            getRequest(id);
            getMessages(id);
        }

        initMessage();
        getUsers();

        function delegateRequest(req) {
            return request.delegateRequest(req, users)
                .then(function() {
                    return request.patch(req.id, {
                        resolution: null
                    });
                })
                .then(function() {
                    getRequest(id);
                })
                .catch(function(err) {
                    console.log(err);
                });
        }

        function getUsers() {
            return user.getAll(true)
                .then(function(res) {
                    users = res;
                });
        }

        function initMessage() {
            vm.message = {
                user_id: locker.get('user').id
            };
        }

        function getRequest(id) {
            return request.get(id)
                .then(function(res) {
                    vm.request = res;
                });
        }

        function getMessages(id) {
            return request.getMessages(id)
                .then(function(res) {
                    vm.messages = res;
                });
        }

        function saveMessage() {
            if (vm.message && vm.message.message) {
                vm.message.request_id = vm.request.id;
                request.saveMessage(vm.message)
                    .then(function(res) {
                        if (res && res.rows) {
                            toastr.success(res.msg, 'Éxito');
                            vm.messages.push(res.rows);
                            initMessage();
                        }
                    })
                    .catch(function(err) {
                        toastr.error(err, 'Error');
                    });
            }
        }

        function resolveModal(req) {
            return request.resolveRequest(req)
                .then(function() {
                    getRequest(vm.request.id);
                });
        }

        function respondRequest(req) {
            return request.respondRequest(req)
                .then(function(res) {
                    getRequest(vm.request.id);
                });
        }

        function displayImage(uniqueid) {
            return request.displayImage(uniqueid);
        }

    });
