'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:CategoryCtrl
 * @description
 * # CategoryCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.category', {
                url: 'category',
                controller: 'CategoryCtrl as vm',
                templateUrl: 'views/category.html',
                loginRequired: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Categoría'
                },
                resolve: {
                    _categories: function(category) {
                        return category.getAll();
                    }
                }
            });
    })
    .controller('CategoryCtrl', function($state, _categories, category) {
        var vm = this;
        vm.tituloPagina = $state.current.data.title;
        vm.reload = getCategories;
        vm.categories = _categories;

        function getCategories() {
            return category.getAll()
                .then(function(res) {
                    vm.categories = res;
                });
        }

    });
