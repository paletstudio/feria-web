'use strict';

/**
 * @ngdoc directive
 * @name feriaApp.directive:evTable
 * @description
 * # evTable
 */
angular.module('feriaApp')
    .directive('evTable', EvTable);

function EvTable() {
    return {
        templateUrl: 'views/template/evTable.html',
        restrict: 'A',
        scope: {
            collection: '=',
            filter: '=',
            itemsPerPage: '@',
            showAllItems: '=',
            serverPagination: '=?',
            onPaginate: '&?',
            totalItems: '=?'
        },
        controller: EvTableCtrl,
        controllerAs: 'vm',
        bindToController: true,
        transclude: true,
        replace: true,
        link: function postLink(scope, elem, attrs, vm) {
            scope.$watch('vm.collection', function(newValue) {
                if (newValue !== undefined) {
                    vm.paginate(false);
                }
            });

            scope.$watch('vm.filter', function(newValue) {
                vm.puedeFiltrar = false;
                if (angular.isUndefined(newValue) || newValue === true) {
                    vm.puedeFiltrar = true;
                }
            });

            scope.$watch('vm.itemsPerPage', function(newValue) {
                vm.pagination.itemsPerPage = 10;
                if (angular.isDefined(newValue) && angular.isNumber(parseInt(newValue))) {
                    vm.pagination.itemsPerPage = parseInt(newValue);
                }
            });

            scope.$watch('vm.showAllItems', function(newValue) {
                if (angular.isDefined(newValue) && newValue === false) {
                    vm.pagination.itemsPerPageOptions.splice(4, 1);
                }
            });

        }
    };
}

EvTableCtrl.$inject = ['$scope', '$filter'];

function EvTableCtrl($scope, $filter) {
    var vm = this;

    vm.pagination = {};
    vm.pagination.page = 1;
    vm.pagination.itemsPerPageOptions = [{
        'id': 1,
        'name': '10',
        'value': 10
    }, {
        'id': 2,
        'name': '25',
        'value': 25
    }, {
        'id': 3,
        'name': '50',
        'value': 50
    }, {
        'id': 4,
        'name': '100',
        'value': 100
    }, {
        'id': 5,
        'name': 'Todos',
        'value': 10000000000
    }];
    vm.pagination.itemsPerPage = vm.itemsPerPage || vm.pagination.itemsPerPageOptions[0].value;
    vm.paginate = paginate;

    //Esta funcion pagina y filtra cada se llama
    function paginate(withNgChange) {
        //rows es la coleccion de elementos
        var rows = vm.collection || [];
        var filteredRows = [];
        filteredRows = $filter('filter')(rows, vm.pagination.search || '');
        //
        vm.pagination.totalRows = vm.serverPagination ? vm.totalItems : filteredRows.length;

        if (!vm.serverPagination) {
            if (vm.pagination.itemsPerPage !== 'Todos') {
                if(filteredRows.length !== rows.length){
                    vm.pagination.page = 1;
                }
                var start = (vm.pagination.page - 1) * vm.pagination.itemsPerPage;
                var end = start + vm.pagination.itemsPerPage;
                filteredRows = filteredRows.slice(start, end);
            }
        }

        if (withNgChange && vm.serverPagination && vm.onPaginate) {
            $scope.$parent.$event = {
                page: vm.pagination.page,
                itemsPerPage: vm.pagination.itemsPerPage
            };
            vm.onPaginate();
        }

        $scope.$parent.$rows = filteredRows;
    }
}
