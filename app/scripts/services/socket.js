'use strict';

/**
 * @ngdoc service
 * @name feriaApp.socket
 * @description
 * # socket
 * Factory in the feriaApp.
 */
angular.module('feriaApp')
    .factory('socket', function(socketFactory) {
        // Service logic
        // ...
        return socketFactory({
            ioSocket: io.connect('http://158.69.202.20:3000')
            //ioSocket: io.connect('http://localhost:3000')
        });
    });
