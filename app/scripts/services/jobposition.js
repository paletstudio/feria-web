'use strict';

/**
 * @ngdoc service
 * @name feriaApp.jobposition
 * @description
 * # jobposition
 * Service in the feriaApp.
 */
angular.module('feriaApp')
    .service('jobposition', function(API, $q, toastr) {

        var me = this;
        var url = 'jobposition';

        /*me.init = function() {
            return {
                id: 0,
                profile_id: 0,
                name: null,
                family_name: null,
                family_name_2: null,
                username: null,
                email: null,
                password: null,
                workingplace_id:0,
                jobposition_id:0,
                active: 1
            };
        };*/

        me.getAll = function() {
            return API.get(url)
                .then(function(res) {
                    if (res.data.rows && res.data.rows.length > 0) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.get = function(id) {
            return API.get(url + '/' + id)
                .then(function(res) {
                    if (res.data.rows && res.data.rows[0]) {
                        return $q.resolve(res.data.rows[0]);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(null);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.create = function(jobposition) {
            return API.post(url, jobposition);
        };

        me.update = function(jobposition) {
            return API.put(url + '/' + jobposition.id, jobposition);
        };

        me.save = function(jobposition) {
            if (jobposition.id) {
                return me.update(jobposition);
            } else {
                return me.create(jobposition);
            }
        };

    });
