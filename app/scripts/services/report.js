'use strict';

/**
 * @ngdoc service
 * @name feriaApp.report
 * @description
 * # report
 * Service in the feriaApp.
 */
angular.module('feriaApp')
    .service('report', function(API, toastr, $q, $http, settings) {

        var me = this;
        var url = 'report';

        me.filterInit = function() {
            return {
                finish_date: null,
                start_date: null,
                status: null,
                category: null,
                subcategory: null,
                request_origin: null,
                employee: null
            };
        };

        function addParam(path, param) {
            path += (path.split('?')[1] ? '&' : '?') + param;
            return path;
        }

        me.generateReport = function(filter) {

            var query = '';

            if (filter.finish_date && filter.start_date) query += addParam(query, 'from=' + moment(filter.start_date).format('YYYY-MM-DD') + '&to=' + moment(filter.finish_date).format('YYYY-MM-DD'));
            if (filter.status) query = addParam(query, 'status=' + filter.status);
            if (filter.type) query = addParam(query, 'type=' + filter.type);
            if (filter.category) query = addParam(query, 'category=' + filter.category);
            if (filter.subcategory) query = addParam(query, 'subcategory=' + filter.subcategory);
            if (filter.request_origin) query = addParam(query, 'origin=' + filter.request_origin);
            if (filter.employee) query = addParam(query, 'employee=' + filter.employee);
            if (filter.paginate) query = addParam(query, 'paginate=' + true);
            if (filter.page) query = addParam(query, 'page=' + filter.page);
            if (filter.items) query = addParam(query, 'items=' + filter.items);

            return API.get(url + '/request' + query)
                .then(function(res) {
                    if (res.data.rows && (res.data.rows.length > 0 || res.data.rows.data.length > 0)) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.exportExcel = function(filter) {
            var query = '';

            if (filter.finish_date && filter.start_date) query += addParam(query, 'from=' + moment(filter.start_date).format('YYYY-MM-DD') + '&to=' + moment(filter.finish_date).format('YYYY-MM-DD'));
            if (filter.status) query = addParam(query, 'status=' + filter.status);
            if (filter.type) query = addParam(query, 'type=' + filter.type);
            if (filter.category) query = addParam(query, 'category=' + filter.category);
            if (filter.subcategory) query = addParam(query, 'subcategory=' + filter.subcategory);
            if (filter.request_origin) query = addParam(query, 'origin=' + filter.request_origin);
            if (filter.employee) query = addParam(query, 'employee=' + filter.employee);
            if (filter.paginate) query = addParam(query, 'paginate=' + true);
            if (filter.page) query = addParam(query, 'page=' + filter.page);
            if (filter.items) query = addParam(query, 'items=' + filter.items);
            //console.log(query);
            var config = {
                method: 'GET',
                url: settings.baseUrl + url + '/request/excel' + query,
                responseType: 'arraybuffer'
            };
            $http(config)
                .then(function(res) {
                    console.log(res.headers());
                    var blob = new Blob([res.data], {
                        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                    });
                    var objectUrl = (window.URL || window.webkitURL).createObjectURL(blob);
                    var link = angular.element('<a/>');
                    link.attr({
                        href: objectUrl,
                        download: 'Request_Report_' + moment().format('YYYY-MM-DD H_m_s') + '.xlsx'
                    })[0].click();
                });
        };


    });
