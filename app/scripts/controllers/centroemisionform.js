'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:CentroemisionformCtrl
 * @description
 * # CentroemisionformCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.centroEmision-new', {
                url: 'centroemision/new',
                controller: 'CentroEmisionFormCtrl as vm',
                templateUrl: 'views/centroemision.form.html',
                loginRequired: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Centro de emision > Nuevo'
                }
            })
            .state('app.centroEmision-edit', {
                url: 'centroemision/{id:int}',
                controller: 'CentroEmisionFormCtrl as vm',
                templateUrl: 'views/centroemision.form.html',
                loginRequired: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Centro de emision > Editar'
                }
            });
    })
    .controller('CentroEmisionFormCtrl', function(centroEmision, $state, toastr, sweetAlert) {
        var vm = this;
        var id = $state.params.id;
        vm.tituloPagina = $state.current.data.title;
        vm.centroEmision = centroEmision.init();

        function getCentroEmision(id) {
            return centroEmision.get(id)
                .then(function(res) {
                    if (res) {
                        vm.centroEmision = res;
                    } else {
                        $state.go('app.centroEmision-new');
                    }
                });
        }

        if (id) {
            getCentroEmision(id);
        }

        vm.save = function() {
            centroEmision.save(vm.centroEmision)
                .then(function(res) {
                    toastr.success(res.data.msg, 'Exito');
                    if (!vm.centroEmision.id) vm.clean();
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                });
        };

        vm.clean = function() {
            vm.centroEmision = centroEmision.init();
        };

        vm.delete = function() {
            sweetAlert.confirm('¿Está seguro?')
                .then(function() {
                    centroEmision.delete(vm.centroEmision)
                        .then(function(res) {
                            $state.go('app.centroEmision');
                            toastr.success(res.data.msg, 'Éxito');
                        });
                });
        };

    });
