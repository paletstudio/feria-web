'use strict';

/**
 * @ngdoc directive
 * @name sytevApp.directive:formFieldNumber
 * @description
 * # formFieldNumber
 */
angular.module('feriaApp')
    .directive('formFieldNumber', function() {
        var template = '<div class="form-group" ng-class="{\'has-error\' : form.{{name}}.$invalid && ngRequired, \'has-success\':form.{{name}}.$valid && ngRequired }">';
        template += '<label ng-class="{\'col-md-3\' : horizontal, \'control-label\': true}">{{label}}</label>';
        template += '<div ng-class="{\'col-md-9\' : horizontal}">';
        template += '<input pattern="{{pattern}}" min="{{min}}" max="{{max}}" ng-model="ngModel" ng-required="ngRequired" ng-disabled="ngDisabled" name="{{name}}" type="number" placeholder="{{placeholder}}" maxlength="{{maxlength}}" class="form-control">';
        template += '</div>';
        template += '</div>';
        return {
            template: template,
            restrict: 'E',
            scope: {
                name: '@',
                ngModel: '=ngModel',
                formStyle: '@',
                label: '@',
                placeholder: '@',
                min: '@',
                max: '@',
                ngRequired: '=?',
                ngShow: '=?',
                ngHide: '=?',
                ngIf: '=?',
                ngDisabled: '=?',
                ngTrim: '=?',
                pattern: '@',
                ngChange: '&',
                maxlength: '@'
            },
            require: ['^form', 'ngModel'],
            link: function postLink(scope, element, attrs, controllers) {
                scope.form = controllers[0];
                scope.horizontal = true;
                scope.placeholder = scope.label;

                scope.$watch('ngModel', function(value) {
                    if(!angular.isNumber(value)){
                        scope.ngModel = Number(value);
                    }
                    if (scope.ngChange) {
                        scope.ngChange();
                    }
                });

                attrs.$observe('formStyle', function(value) {
                    if (value === 'vertical') {
                        scope.horizontal = false;
                    }
                });

                attrs.$observe('placeholder', function(value) {
                    scope.placeholder = value;
                });
            }
        };
    });
