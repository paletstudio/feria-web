'use strict';

/**
 * @ngdoc service
 * @name feriaApp.requestorigin
 * @description
 * # requestorigin
 * Service in the feriaApp.
 */
angular.module('feriaApp')
    .service('requestorigin', function(API, toastr, $q) {
        var me = this;
        var url = 'requestorigin';

        me.getAll = function() {
            return API.get(url)
                .then(function(res) {
                    if (res.data.rows && res.data.rows.length > 0) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

        me.getVisibles = function() {
            return API.get(url + '?info=true')
                .then(function(res) {
                    if (res.data.rows && res.data.rows.length > 0) {
                        return $q.resolve(res.data.rows);
                    } else {
                        toastr.info(res.data.msg, 'Atención');
                        return $q.resolve(res.data.rows);
                    }
                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });
        };

    });
