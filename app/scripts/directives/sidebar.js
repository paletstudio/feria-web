'use strict';

/**
 * @ngdoc directive
 * @name sytevApp.directive:sidebar
 * @description
 * # sidebar
 */
angular.module('feriaApp')
    .directive('menuLink', function() {
        var template = '<a ui-sref="{{opcion.estado}}"><i class="{{opcion.icono}}"></i> {{opcion.descripcion}}</a>';

        return {
            template: template,
            replace: true,
            restrict: 'E',
            scope: {
                opcion: '='
            }
        };
    })
    .directive('menuToggle', function() {
        var template = '<div>';
        template += '<a href style="text-decoration:none" ng-click="ngClick">';
        template += '<i class="{{opcion.icono}}"></i> {{opcion.descripcion}}';
        template += '<span class="fa arrow" ng-class="{\'fa-angle-left\': collapsed!=itemNumber, \'fa-angle-down\' : collapsed==itemNumber}"></span>';
        template += '</a>';
        template += '<ul class="nav nav-second-level" uib-collapse="collapsed!=itemNumber">';
        template += '<li ng-repeat="o in opcion.opciones" ui-sref-active="active" >';
        template += '<menu-link opcion="o"></menu-link>';
        template += '</li>';
        template += '</ul>';
        template += '</div>';

        return {
            template: template,
            restrict: 'E',
            replace: true,
            scope: {
                opcion: '=',
                itemNumber: '@',
                ngClick: '&',
                collapsed: '='
            }
        };
    })
    .directive('sidebar', function($rootScope, locker) {

        var opciones;
        var user = locker.get('user');

        function obtenerOpciones() {
            opciones = localStorage.getObject('menu') || [];
            opciones = opciones.filter(function(o) {
                return o.en_menu;
            });
        }

        /*obtenerOpciones();*/

        //$rootScope.$on('logged', obtenerOpciones);

        return {
            templateUrl: 'views/main/sidebar.html',
            restrict: 'E',
            replace: true,
            scope: {},
            controller: function($scope) {
                $scope.opciones = opciones;
                $scope.user = user;
                $scope.collapsed = 0;
                $scope.check = function(x) {
                    x = parseInt(x);
                    if (x === $scope.collapsed) {
                        $scope.collapsed = 0;
                    } else {
                        $scope.collapsed = x;
                    }
                };
            }
        };

    });
