'use strict';

describe('Service: oxxo', function () {

  // load the service's module
  beforeEach(module('feriaApp'));

  // instantiate service
  var oxxo;
  beforeEach(inject(function (_oxxo_) {
    oxxo = _oxxo_;
  }));

  it('should do something', function () {
    expect(!!oxxo).toBe(true);
  });

});
