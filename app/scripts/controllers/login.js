'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .controller('LoginCtrl', function($state, session) {
        var vm = this;
        vm.login = login;

        function login() {
            session.login(vm.username, vm.password)
                .then(function() {
                    $state.go('app.inicio');
                });
        }
    });
