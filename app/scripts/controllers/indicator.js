'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:IndicatorctrlCtrl
 * @description
 * # IndicatorctrlCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.indicator', {
                url: 'indicator',
                controller: 'IndicatorCtrl as vm',
                templateUrl: 'views/indicator.html',
                loginRequired: true,
                blacklist: [
                    'user'
                ],
                data: {
                    title: 'Indicadores'
                }
            });
    });

angular.module('feriaApp')
    .controller('IndicatorCtrl', function($state, indicator, session) {
        var vm = this;
        vm.tituloPagina = $state.current.data.title;
        vm.chart = {};
        vm.chart2 = {};
        vm.chart3 = {};
        vm.chart4 = {};
        vm.reload = activate;
        activate();

        function activate() {
            indicator.requestStatus()
                .then(function(res) {
                    if (res) {
                        var chart = indicator.dataToGraphic(res, 'Estatus', 'Cantidad');
                        vm.chart = {};
                        vm.chart.labels = chart.labels;
                        vm.chart.data = chart.data;
                        vm.chart.total = chart.total;
                        vm.chart.options = {
                            responsive: true,
                            maintainAspectRatio: false,
                        };
                    }
                });

            indicator.category()
                .then(function(res) {
                    if (res) {
                        var chart2 = indicator.dataToGraphic(res, 'Categoria', 'Cantidad');
                        vm.chart2 = {};
                        vm.chart2.labels = chart2.labels;
                        vm.chart2.data = chart2.data;
                        vm.chart2.total = chart2.total;
                        vm.chart2.series = ['Cantidad'];
                        vm.chart2.options = {
                            responsive: true,
                            maintainAspectRatio: false,
                        };
                    }
                });

                indicator.category({
                    type: 2
                })
                    .then(function(res) {
                        if (res) {
                            var chart4 = indicator.dataToGraphic(res, 'Categoria', 'Cantidad');
                            vm.chart4 = {};
                            vm.chart4.labels = chart4.labels;
                            vm.chart4.data = chart4.data;
                            vm.chart4.total = chart4.total;
                            vm.chart4.series = ['Cantidad'];
                            vm.chart4.options = {
                                responsive: true,
                                maintainAspectRatio: false,
                            };
                        }
                    });

            if (session.isInRole('admin')) {
                indicator.categoryAverage()
                    .then(function(res) {
                        if (res) {
                            var chart3 = indicator.dataToGraphic(res, 'Categoria', 'Promedio');
                            vm.chart3 = {};
                            vm.chart3.labels = chart3.labels;
                            vm.chart3.data = [chart3.data];
                            vm.chart3.total = chart3.total;
                            vm.chart3.series = ['Promedio en min'];
                            vm.chart3.options = {
                                responsive: true,
                                maintainAspectRatio: false,
                                legend: {
                                    display: false
                                },
                                scales: {
                                    yAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Tiempo en minutos'
                                        }
                                    }],
                                    xAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Categoria'
                                        }
                                    }]
                                }
                            };
                        } else {
                            vm.chart3 = null;
                        }
                    });
            }

        }

    });
