'use strict';

describe('Service: jobposition', function () {

  // load the service's module
  beforeEach(module('feriaApp'));

  // instantiate service
  var jobposition;
  beforeEach(inject(function (_jobposition_) {
    jobposition = _jobposition_;
  }));

  it('should do something', function () {
    expect(!!jobposition).toBe(true);
  });

});
