'use strict';

describe('Directive: formFieldTextarea', function () {

  // load the directive's module
  beforeEach(module('feriaApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<form-field-textarea></form-field-textarea>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the formFieldTextarea directive');
  }));
});
