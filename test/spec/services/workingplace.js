'use strict';

describe('Service: workingplace', function () {

  // load the service's module
  beforeEach(module('feriaApp'));

  // instantiate service
  var workingplace;
  beforeEach(inject(function (_workingplace_) {
    workingplace = _workingplace_;
  }));

  it('should do something', function () {
    expect(!!workingplace).toBe(true);
  });

});
