'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:OxxoCtrl
 * @description
 * # OxxoCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.oxxo', {
                url: 'oxxo',
                controller: 'OxxoCtrl as vm',
                templateUrl: 'views/oxxo.html',
                loginRequired: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Oxxo'
                },
                resolve: {
                    _oxxo: function(oxxo){
                        return oxxo.getAll();
                    }
                }
            });
    })
    .controller('OxxoCtrl', function(oxxo, $state, _oxxo) {
        var vm = this;
        vm.tituloPagina = $state.current.data.title;
        vm.reload = getOxxos;
        vm.oxxos = _oxxo;

        function getOxxos() {
            return oxxo.getAll()
                .then(function(res) {
                    vm.oxxos = res;
                });
        }

    });
