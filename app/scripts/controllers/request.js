'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:RequestCtrl
 * @description
 * # RequestCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.request', {
                url: 'request?status',
                controller: 'RequestCtrl as vm',
                templateUrl: 'views/request.html',
                loginRequired: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Sugerencias'
                },
                resolve: {
                    _request: function(request, $stateParams) {
                        return request.getAll({
                            info: true,
                            type: 1,
                            paginate: true,
                            status: $stateParams.status
                        });
                    },
                    _user: function(user) {
                        return user.getAll(true);
                    },
                    _status: function(status) {
                        return status.getAll();
                    },
                    _types: function(request) {
                        return request.getRequestTypes();
                    },
                }
            });
    })
    .controller('RequestCtrl', function(request, $state, toastr, user, $sce, $scope, _request, _user, _status, _types, socket, session) {
        var vm = this;
        var users = _user;
        // var statusCollection = [1, 2, 3];
        vm.tituloPagina = $state.current.data.title;
        vm.delegateRequest = delegateRequest;
        vm.reload = getRequests;
        vm.filterRequests = filterRequests;
        vm.requests = _request.data;
        vm.totalItems = _request.total;
        vm.status = _status;
        vm.types = _types;
        vm.status.unshift({
            id: '',
            name: 'Todos'
        });
        vm.filter = {};
        vm.filter.type = 1;
        vm.requestUserInfo = request.requestUserInfo;
        vm.respondRequest = respondRequest;
        vm.paginate = paginate;

        if ($state.params.status) {
            vm.filter.status = parseInt($state.params.status);
            addTooltip();
        }

        function addTooltip() {
            vm.requests = angular.forEach(vm.requests, function(r) {
                r.tooltip = request.showTooltip(r);
            });
        }

        function filterRequests() {
            getRequests({ status: vm.filter.status, type: vm.filter.type});
        }

        function delegateRequest(req) {
            return request.delegateRequest(req, users)
                .then(function() {
                    return request.patch(req.id, {
                        resolution: null
                    });
                })
                .then(function() {
                    getRequests();
                });
        }

        function respondRequest(req) {
            return request.respondRequest(req)
                .then(function() {
                    getRequests();
                });
        }

        function getRequests(params) {
            return request.getAll({
                    info: true,
                    type: params.type || 1,
                    paginate: true,
                    page: params.page || 1,
                    itemsPerPage: params.itemsPerPage || 10,
                    status: params.status || null
                })
                .then(function(res) {
                    vm.requests = res.data;
                    vm.totalItems = res.total;
                    addTooltip();
                });
        }

        // function updateRequestList(data) {
        //     vm.requests = [data].concat(vm.requests);
        // }

        function paginate(data) {
            getRequests(data);
        }

        // socket.on('request.new', function(data) {
        //     if (session.isInRole('admin')) {
        //         updateRequestList(data);
        //     }
        // });

    });
