'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:InicioCtrl
 * @description
 * # InicioCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.inicio', {
                url: '',
                controller: 'InicioCtrl as inicio',
                templateUrl: 'views/inicio.html',
                loginRequired: true,
                validateAccess: true
            });
    })
    .controller('InicioCtrl', function() {});
