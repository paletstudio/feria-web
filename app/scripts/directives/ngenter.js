'use strict';

/**
 * @ngdoc directive
 * @name feriaApp.directive:ngEnter
 * @description
 * # ngEnter
 */
angular.module('feriaApp')
    .directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind('keydown keypress', function(event) {
                if (event.which === 13) {
                    scope.$apply(function() {
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    });
