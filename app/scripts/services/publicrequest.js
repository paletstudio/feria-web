'use strict';

/**
 * @ngdoc service
 * @name feriaApp.request
 * @description
 * # request
 * Service in the feriaApp.
 */
angular.module('feriaApp')
    .service('publicrequest', function(API, $uibModal, $q, toastr) {
        var me = this;
        var url = 'public/request';

        me.init = function() {
            return {
                id: 0,
                applicant_name: null,
                applicant_family_name: null,
                applicant_family_name_2: null,
                applicant_phone: null,
                applicant_email: null,
                //applicant_identifier: null,
                // subject: null,
                // description: null,
                // event_date: null,
                // application_datetime: null,
                subcategory_id: 0,
                // subcategory_value_1: null,
                // subcategory_value_2: null,
                status_id: 0,
                user_id: 0,
                requestorigin_id: 0,
                response: 0,
                active: 1,
                data: []
            };
        };

        me.create = function(request) {
            return API.post(url, request, {
                skipAuthorization: true
            });
        };

    });
