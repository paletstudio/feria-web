(function() {
    'use strict';

    angular
        .module('feriaApp')
        .controller('RespondRequestCtrl', RespondRequestCtrl);

    RespondRequestCtrl.$inject = ['$scope', '_request', 'request', '$uibModalInstance', 'locker', 'lodash', 'toastr'];

    function RespondRequestCtrl($scope, _request, request, $uibModalInstance, locker, lodash, toastr) {
        /* @ngInject */
        var vm = this;
        vm.request = _request;
        vm.cancel = cancel;
        vm.respondRequest = respondRequest;

        function cancel() {
            return request.patch(vm.request.id, {
                    status_id: 3
                })
                .then(function() {
                    return $uibModalInstance.dismiss(null);
                });
        }

        function respondRequest() {
            var req = angular.copy(vm.request);
            req.status_id = 4;
            req.resolve_user_id = locker.get('user').id;
            req = lodash.omit(req, ['status', 'category', 'subcategory', 'origin']);

            return request.save(req)
                .then(function(res) {
                    console.log(res);
                    $scope.$emit('request.resolve');
                    $uibModalInstance.close();
                    toastr.success(res.data.msg, 'Éxito');

                })
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return cancel();
                });

        }
    }
})();
