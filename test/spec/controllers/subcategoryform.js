'use strict';

describe('Controller: SubcategoryformCtrl', function () {

  // load the controller's module
  beforeEach(module('feriaApp'));

  var SubcategoryformCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SubcategoryformCtrl = $controller('SubcategoryformCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(SubcategoryformCtrl.awesomeThings.length).toBe(3);
  });
});
