'use strict';

/**
 * @ngdoc service
 * @name feriaApp.session
 * @description
 * # session
 * Service in the feriaApp.
 */
angular.module('feriaApp')
    .service('session', function($state, API, $q, settings, $rootScope, locker, jwtHelper, toastr, socket) {

        var me = this;

        me.login = function(username, password) {
            return API.post('login', {
                    username: username,
                    password: password,
                }, {
                    skipAuthorization: true
                })
                .then(function(res) {
                    var token = res.data.token;
                    storeSessionData(token);
                    socket.connect();

                    return $q.when();
                    //return me.obtenerMenus(res.data.Datos.id_perfil, idSistema);
                })
                /*.then(function(res) {
                    localStorage.setObject('menu', res.data.Datos);
                    $rootScope.$emit('logged');

                })*/
                .catch(function(err) {
                    toastr.error(err, 'Error');
                    return $q.reject(err);
                });

        };

        me.logout = function() {
            return API.post('logout')
                .catch(function(err) {
                    toastr.error('Sesion expirada', 'Error', {
                        preventDuplicates: true
                    });

                })
                .finally(function() {
                    removeSessionData();
                    $state.go('login');
                    socket.disconnect();
                });
        };

        me.isLogged = function() {
            var token = locker.get('token');
            var user = locker.get('user');

            return token && user;

            /*return token && !jwtHelper.isTokenExpired(token);*/
        };

        me.isInRole = function(value) {
            var string = value;
            value = $rootScope.$eval(value);
            if (!angular.isArray(value) && angular.isUndefined(value)) {
                value = [string];
            }
            var token = locker.get('token');
            var tokenPayload = jwtHelper.decodeToken(token);

            if (tokenPayload.role && value.indexOf(tokenPayload.role) > -1) {
                return true;
            } else {
                return false;
            }
        };

        me.getLoggedinUser = function() {
            return locker.get('user');
        };

        function storeSessionData(token) {
            var tokenPayload = jwtHelper.decodeToken(token);
            var user = tokenPayload.user;

            locker.put('token', token);
            locker.put('user', user);
        }

        function removeSessionData() {
            locker.clean();
        }

        /*me.obtenerMenus = function(idPerfil, idSistema) {
            return $http.get(url + 'opcion/perfil/' + idPerfil + '/sistema/' + idSistema);
        };*/
    });
