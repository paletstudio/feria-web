'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:RutaCtrl
 * @description
 * # RutaCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.ruta', {
                url: 'ruta',
                controller: 'RutaCtrl as vm',
                templateUrl: 'views/ruta.html',
                loginRequired: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Ruta'
                },
                resolve: {
                    _ruta: function(ruta){
                        return ruta.getAll();
                    }
                }
            });
    })
    .controller('RutaCtrl', function(ruta, $state, _ruta) {
        var vm = this;
        vm.tituloPagina = $state.current.data.title;
        vm.reload = getRutas;
        vm.rutas = _ruta;

        function getRutas() {
            return ruta.getAll()
                .then(function(res) {
                    vm.rutas = res;
                });
        }

    });
