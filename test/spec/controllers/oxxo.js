'use strict';

describe('Controller: OxxoCtrl', function () {

  // load the controller's module
  beforeEach(module('feriaApp'));

  var OxxoCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OxxoCtrl = $controller('OxxoCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
