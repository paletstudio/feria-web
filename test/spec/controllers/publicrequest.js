'use strict';

describe('Controller: PublicrequestCtrl', function () {

  // load the controller's module
  beforeEach(module('feriaApp'));

  var PublicrequestCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PublicrequestCtrl = $controller('PublicrequestCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PublicrequestCtrl.awesomeThings.length).toBe(3);
  });
});
