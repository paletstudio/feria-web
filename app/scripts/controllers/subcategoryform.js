'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:SubsubcategoryformCtrl
 * @description
 * # SubsubcategoryformCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.subcategory-new', {
                url: 'subcategory/new',
                controller: 'SubcategoryFormCtrl as vm',
                templateUrl: 'views/subcategory.form.html',
                loginRequired: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Subcategoría > Nueva'
                }
            })
            .state('app.subcategory-edit', {
                url: 'subcategory/{id:int}',
                controller: 'SubcategoryFormCtrl as vm',
                templateUrl: 'views/subcategory.form.html',
                loginRequired: true,
                blacklist: [
                    'user', 'auditor'
                ],
                data: {
                    title: 'Subcategoría > Editar'
                }
            });
    })
    .controller('SubcategoryFormCtrl', function(subcategory, $state, category, toastr, sweetAlert) {
        var vm = this;
        var id = $state.params.id;
        vm.tituloPagina = $state.current.data.title;
        vm.subcategory = subcategory.init();
        vm.clean = clean;
        vm.save = save;

        function getSubcategory(id) {
            return subcategory.get(id)
                .then(function(res) {
                    if (res) {
                        vm.subcategory = res;
                    } else {
                        $state.go('app.subcategory-new');
                    }
                });
        }

        function getCategories() {
            category.getAll()
                .then(function(res) {
                    vm.categories = res;
                });
        }

        getCategories();

        if (id) {
            getSubcategory(id);
        }

        function save() {
            subcategory.save(vm.subcategory)
                .then(function(res) {
                    toastr.success(res.data.msg);
                    if (!vm.subcategory.id) clean();
                })
                .catch(function(err) {
                    toastr.error(err);
                });
        }

        function clean() {
            vm.subcategory = subcategory.init();
        }

        vm.delete = function() {
            sweetAlert.confirm('¿Está seguro?')
            .then(function() {
                subcategory.delete(vm.subcategory)
                .then(function(res) {
                    toastr.success(res.data.msg, 'Éxito');
                    $state.go('app.subcategory');
                });
            })
        }

    });
