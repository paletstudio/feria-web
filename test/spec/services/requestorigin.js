'use strict';

describe('Service: requestorigin', function () {

  // load the service's module
  beforeEach(module('feriaApp'));

  // instantiate service
  var requestorigin;
  beforeEach(inject(function (_requestorigin_) {
    requestorigin = _requestorigin_;
  }));

  it('should do something', function () {
    expect(!!requestorigin).toBe(true);
  });

});
