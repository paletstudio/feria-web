'use strict';

describe('Controller: UserrouteCtrl', function () {

  // load the controller's module
  beforeEach(module('feriaApp'));

  var UserrouteCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UserrouteCtrl = $controller('UserrouteCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
