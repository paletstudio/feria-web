'use strict';

/**
 * @ngdoc function
 * @name feriaApp.controller:RequestformCtrl
 * @description
 * # RequestformCtrl
 * Controller of the feriaApp
 */
angular.module('feriaApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('app.request-new-internal', {
                url: 'request/new/internal',
                controller: 'RequestFormInternalCtrl as vm',
                templateUrl: 'views/request.form.internal.html',
                loginRequired: true,
                /*
                               validateAccess: true,
                               cache: false,*/
                data: {
                    title: 'Sugerencia Interna > Nueva'
                }
            });
    })
    .controller('RequestFormInternalCtrl',
        function(request, $state, moment, toastr, subcategory, category, centroEmision, oxxo, ruta, estacion, locker, $q) {
            var vm = this;
            var id = $state.params.id;
            var subcategories = [];
            var selectedCategory;
            var user;

            vm.maxDate = moment().toDate();
            vm.minDate = moment().subtract(3, 'month').toDate();
            vm.dpOpened = false;
            vm.tituloPagina = $state.current.data.title;
            vm.request = request.init();
            vm.identifierOptions = request.getIdentifierOptions();
            vm.identifier = vm.identifierOptions[1];
            vm.categoryChanged = categoryChanged;
            vm.clean = clean;
            vm.save = save;
            vm.displayImage = displayImage;

            if (id) {
                getRequest(id)
                    .then(function() {
                        return activate();
                    })
                    .then(function() {
                        vm.request.category = vm.categories.find(function(c) {
                            return vm.request.category.id === c.id;
                        });
                        vm.subcategories = subcategories.filter(function(s) {
                            return s.category_id === vm.request.category.id;
                        });
                    });
            }

            activate();

            vm.prepareRequestData = function() {
                vm.request.data = vm.request.issue.data.map(function(d) {
                    return {
                        data: d
                    };
                });
            };

            function activate() {
                fillUser();
                return $q.all([getCategories()])
                    .then(function(res) {
                        return res;
                    });

            }

            function fillUser() {
                user = locker.get('user');
                vm.request.applicant_name = user.name;
                vm.request.applicant_family_name = user.family_name;
                vm.request.applicant_family_name_2 = user.family_name_2;
                vm.request.applicant_email = user.email;
            }

            function categoryChanged() {
                vm.request.data = [];
                vm.request.subcategory = null;
            }

            function getRequest(id) {
                return request.get(id)
                    .then(function(res) {
                        if (res) {
                            vm.request = res;
                            vm.request.event_date = moment(vm.request.event_date).toDate();
                        } else {
                            $state.go('app.request-new');
                            return $q.reject();
                        }
                    });
            }

            function getCategories() {
                return category.getAll({
                    type: 2,
                    info: true
                })
                    .then(function(res) {
                        vm.categories = res;
                    });
            }

            function clean() {
                vm.request = request.init();
                fillUser();
            }

            function save() {
                setNotVisibleValues();
                // console.log(vm.request);
                request.save(vm.request)
                    .then(function(res) {
                        toastr.success(res.data.msg, 'Éxito');
                        if (!vm.request.id) clean();
                    })
                    .catch(function(err) {
                        toastr.error(err, 'Error');
                    });
            }

            function setNotVisibleValues() {
                vm.request.application_datetime = moment().format();
                vm.request.subcategory_id = vm.request.subcategory.id;
                vm.request.issue_id = vm.request.issue.id;
                vm.request.status_id = 1;
                vm.request.applicant_is_folio = vm.identifier.id;
                vm.request.user_id = locker.get('user').id;
                vm.request.requestorigin_id = locker.get('user').requestorigin;
                vm.request.request_type_id = 2;
            }

            function displayImage(uniqueid) {
                return request.displayImage(uniqueid);
            }

        });
